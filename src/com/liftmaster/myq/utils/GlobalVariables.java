package com.liftmaster.myq.utils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;

public class GlobalVariables {

	public static AndroidDriver<AndroidElement> androidDriver = null;
	public static IOSDriver<IOSElement> iOSDriver = null;
	public static String currentTestIOS = null;
	public static String currentTestAndroid = null;

	/**
	 * All variables for service URL's
	 */
	public static String GetDeviceListWebserviceURL = "http://myqsociety-pp-v2.centralus.cloudapp.azure.com:8155/api/devices/";
	public static String CreateGatewayServiceURL = "http://myqsociety-pp-v2.centralus.cloudapp.azure.com:8155/api/gateways/";
	public static String SendGatewayOnlineWebserviceURL = "http://myqsociety-pp-v2.centralus.cloudapp.azure.com:8155/api/gateways/";
	public static String AddLightWebserviceURL = "http://myqsociety-pp-v2.centralus.cloudapp.azure.com:8155/api/lights/";
	public static String AddGDOWebserviceURL = "http://myqsociety-pp-v2.centralus.cloudapp.azure.com:8155/api/garageDoorOpeners/";
	public static String GDOWebserviceURL = "http://myqsociety-pp-v2.centralus.cloudapp.azure.com:8155/api/garageDoorOpeners/";
	public static String AddCDOWebserviceURL = "http://myqsociety-pp-v2.centralus.cloudapp.azure.com:8155/api/commercialDoorOpeners/";
	public static String CDOWebserviceURL = "http://myqsociety-pp-v2.centralus.cloudapp.azure.com:8155/api/commercialDoorOpeners/";
	public static String AddGateWebserviceURL = "http://myqsociety-pp-v2.centralus.cloudapp.azure.com:8155/api/gates/";
	public static String GateWebserviceURL = "http://myqsociety-pp-v2.centralus.cloudapp.azure.com:8155/api/gates/";
	public static String LightWebserviceURL = "http://myqsociety-pp-v2.centralus.cloudapp.azure.com:8155/api/lights/";
	
	/**
	 * All variables used for headers,
	 */
	public static String AuthorizationCode = "Bearer 70533feb-b029-4c84-a7ee-05ac2408e15c";
}
