package com.liftmaster.myq.utils;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.TimeZone;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class UtilityIOS {

	public IOSDriver<IOSElement> iOSDriver;
	ExtentTest extentTestIOS;
	static String elementName = null;

	@iOSFindBy(className = "XCUIElementTypePickerWheel")
	public static List<IOSElement> picker_wheel;

	/**
	 * Description Driver Initialize
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public UtilityIOS(IOSDriver<IOSElement> iOSDriver, ExtentTest extentTestIOS) {		
		this.iOSDriver = iOSDriver;
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);
		this.extentTestIOS = extentTestIOS;
	}

	/**
	 * Description this function is used click on element
	 * @param element
	 */
	public void clickIOS(MobileElement element) {
		try {
			elementName = element.getAttribute("label");
			element.click();
			if(elementName!=null) {
			extentTestIOS.log(Status.INFO, "Clicked on : "+elementName);
			}
			else{
			extentTestIOS.log(Status.INFO, "Clicked on : "+element);
			}
		} catch (Exception e) {
			extentTestIOS.log(Status.FAIL, "Failed to click on : " +elementName);
			catchExceptions(iOSDriver, e);
		}
	}

	/**
	 * Description this function is used clear the field
	 * @param ele
	 */
	public void clearIOS(IOSElement ele) {
		try {
			elementName = ele.getAttribute("value");
			ele.clear();
			extentTestIOS.log(Status.INFO, "field clear for : " + ele);
		} catch (NoSuchElementException e) {
			extentTestIOS.log(Status.FAIL, "Unable to clear field for : " + ele);
			catchExceptions(iOSDriver, e);
		}
	}

	/**
	 * Description this function is used enter text into the field
	 * @param ele
	 * @param text
	 */
	public void enterTextIOS(IOSElement ele , String text) {
		try {
			elementName = ele.getAttribute("value");
			ele.sendKeys(text);
			extentTestIOS.log(Status.INFO, "Text entered: " +text );
		} catch (Exception e ) {
			extentTestIOS.log(Status.FAIL, "Failed to entered text in field : " +ele);
			catchExceptions(iOSDriver, e);
		}
	}

	/**
	 * Description this function is used to swipe in left direction
	 * @param ele
	 */
	public void swipeLeft(MobileElement ele) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) iOSDriver;
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("direction", "left");
			scrollObject.put("element", ((IOSElement) ele).getId());
			js.executeScript("mobile: swipe", scrollObject);
			extentTestIOS.log(Status.INFO, "Swiped on element - " +ele );
		} catch (Exception e) {
			catchExceptions(iOSDriver, e);
		}
	}

	/**
	 * Description this function is used to swipe in any direction
	 * @param ele
	 */
	public void swipe(MobileElement ele, String direction) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) iOSDriver;
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("direction", direction);
			scrollObject.put("element", ((IOSElement) ele).getId());
			js.executeScript("mobile: swipe", scrollObject);
			extentTestIOS.log(Status.INFO, "Swiped on element - " +ele+ "in" +direction+ " Direction" );
		} catch (Exception e) {
			catchExceptions(iOSDriver, e);
		}
	}

	/**
	 * Description : This function is used to scroll in view based on direction
	 * @param direction
	 */
	public void scroll(String direction) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) iOSDriver;
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("direction", direction);
			js.executeScript("mobile: scroll", scrollObject);
		} catch (Exception e) {
			catchExceptions(iOSDriver, e);
		}
	}


	/**
	 * Description this function wait for the element to be visible/Clickable/alert to be Present
	 * @param ele
	 * @param state
	 */
	public void waitForElementIOS(MobileElement ele,String state) {
		try{
			WebDriverWait wait = new WebDriverWait(iOSDriver, 30);
			if (state.equals("visibility")) {
				wait.until(ExpectedConditions.visibilityOf(ele));
			} else if (state.equals("clickable")){
				wait.until(ExpectedConditions.elementToBeClickable(ele));	
			} else if (state.equals("alert")){
				wait.until(ExpectedConditions.alertIsPresent());	
			}
		} catch (Exception e ) {
			extentTestIOS.log(Status.FAIL, "The element is not present or clickable before timeout : " +ele);
			catchExceptions(iOSDriver, e);
		}
	}

	/**
	 * Description this function captures screenshot
	 * @param iOSDriver
	 */
	public void captureScreenshot(IOSDriver<IOSElement> iOSDriver) {
		String currentMTA = GlobalVariables.currentTestIOS;
		try {
			File srcFile = ((TakesScreenshot)iOSDriver).getScreenshotAs(OutputType.FILE);
			String fileName = new SimpleDateFormat("dd-MM-yyyy-hhmmss").format(new Date());
			String folderName = System.getProperty("user.dir")+"//Screenshots//";
			FileUtils.copyFile(srcFile, new File(folderName + currentMTA + "-" + fileName+  ".png"));
			extentTestIOS.pass("Screenshot captured -> " + "<a href='file:///"+ folderName + currentMTA + "-" + fileName + ".png'>Screenshot</a>");
			extentTestIOS.log(Status.INFO, "Screenshot captured with file name " + " " + fileName +  ".png"+ " for ticket " + currentMTA );
		} catch(IOException e) { 					//Here specific Exception handling is done to avoid infinite loop
			extentTestIOS.log(Status.FAIL, "Failed to capture Screenshot");
		}
	}

	/**
	 * Description this function captures error messages
	 * @param iOSDriver
	 * @param e
	 */
	public void catchExceptions(IOSDriver<IOSElement> iOSDriver, Exception e) {
		String currentMTA = GlobalVariables.currentTestIOS;
		captureScreenshot(iOSDriver);  
		if (e.getClass().getName().equals("org.openqa.selenium.UnhandledAlertException")) {
			iOSDriver.switchTo().alert().dismiss();
			extentTestIOS.log(Status.INFO, " Unhandled Alert Exception occurred: ");
		} else if (e.getClass().getName().equals("org.openqa.selenium.NoSuchElementException")) {
			extentTestIOS.log(Status.FAIL," No Such Element Exception occurred: " + " Test Case " + currentMTA + " failed");
		} else if (e.getClass().getName().equals("java.lang.NullPointerException")) {
			extentTestIOS.log(Status.FAIL, " Null Pointer Exception occurred: " + " Test Case" + currentMTA + " failed");
		} else if (e.getClass().getName().equals("java.lang.IndexOutOfBoundsException")) {
			extentTestIOS.log(Status.FAIL,  "  Index out of Bound occurred: " + " Test Case" + currentMTA + " failed");
		} else if (e.getClass().getName().equals("org.openqa.selenium.TimeoutException")) {
			extentTestIOS.log(Status.FAIL, " Timeout Exception occurred: " + " Test Case " + currentMTA + " failed");
		} else if (e.getClass().getName().trim().equals("org.openqa.selenium.WebDriverException")) {
			extentTestIOS.log(Status.FAIL, " Webdriver Exception occurred: " + " Test Case " + currentMTA + " failed");
		} else {
			extentTestIOS.log(Status.FAIL, " Exception occurred: " + " Test Case " + currentMTA + " failed");
			e.printStackTrace();
		}
	}

	/**
	 * Description This method enters the Gateway serial number in the text box
	 * @param textBoxLength
	 **/
	public void enterGWSerialNoInTextBox(String textBoxLength) {
		for (int i = 0; i < textBoxLength.length(); i++) {
			iOSDriver.findElementByXPath("//XCUIElementTypeButton[@label = '"+textBoxLength.charAt(i)+"']").click();
		}
	}
}
