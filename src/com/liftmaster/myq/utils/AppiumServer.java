package com.liftmaster.myq.utils;

import java.io.File;
import java.io.IOException;
import org.testng.Assert;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;

public class AppiumServer {

	static String  osName = System.getProperty("os.name"); //Detect the operating system
	static String appiumInstallationDir;
	static AppiumDriverLocalService service = null;

	/**
	 * Description	: This function would start appium server
	 * @param hostIP
	 * @param port
	 */
	public synchronized static void startServer(String hostIP,int port) {

		//location of the project 
		File classPathRoot = new File(System.getProperty("user.dir"));

		//According to the operating system it will launch appium server
		if (osName.contains("Windows")) {
			appiumInstallationDir = "C:\\Users\\tapan.gandhi\\AppData\\Local\\Programs\\appium-desktop\\resources\\app\\node_modules\\appium\\build\\lib\\main.js"; // location of main.js in Windows machine
			service = AppiumDriverLocalService.buildService(new AppiumServiceBuilder() 
					.usingDriverExecutable(new File("C:\\Program Files\\nodejs\\node.exe")) //location of node.exe in Windows machine
					.withAppiumJS(new File(appiumInstallationDir))
					.withIPAddress(hostIP).usingPort(port)
					.withArgument(GeneralServerFlag.SESSION_OVERRIDE)
					.withArgument(GeneralServerFlag.LOCAL_TIMEZONE)
					.withArgument(GeneralServerFlag.LOG_LEVEL,"error")
					.withLogFile(new File(new File(classPathRoot, File.separator + "log"), "AndroidLog.txt")));

		} else if (osName.contains("Mac")) {
			appiumInstallationDir = "/Applications/Appium.app/Contents/Resources/app/node_modules/appium/build/lib/main.js";   // location of main.js in Mac machine
			service = AppiumDriverLocalService
					.buildService(new AppiumServiceBuilder()
							.usingDriverExecutable(new File("/usr/local/bin/node")) // location of the node, to know the location of node run "which node" command on terminal
							.withAppiumJS(new File(appiumInstallationDir))
							.withIPAddress(hostIP).usingPort(port)
							.withArgument(GeneralServerFlag.SESSION_OVERRIDE)
							.withArgument(GeneralServerFlag.LOCAL_TIMEZONE)
							.withArgument(GeneralServerFlag.LOG_LEVEL,"error")
							.withLogFile(new File(new File(classPathRoot, File.separator + "log"), "Log.txt")));
		} else {
			// you can add for other OS, just to track added a fail message
			Assert.fail("Starting appium is not supporting the current OS.");
		}
		service.start();
	}

	/**
	 * Description	: This function would stop appium server
	 */
	public static void stopServer() {

		if (osName.contains("Windows")) {
			String killWindowsNodes = "C:\\windows\\system32\\taskkill /F /IM node.exe"; // kill all the active nodes in windows
			try {
				Runtime.getRuntime().exec(killWindowsNodes);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			String[] killMacNodes = { "/usr/bin/killall", "-KILL", "node" };  // kill all the active nodes in mac
			try {
				Runtime.getRuntime().exec(killMacNodes);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
