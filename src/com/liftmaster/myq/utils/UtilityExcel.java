package com.liftmaster.myq.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;

public class UtilityExcel {

	static Workbook workbook = null;

	//Read Excel TestData File
	public static Recordset ReadExcel(String filePath,String columnName,String sheetName,String filterConditionName,String filetrConditionValue) throws FilloException 
	{ 
		Fillo fillo = new Fillo();
		String strQuery;
		Connection connection = fillo.getConnection(filePath);
		if (filterConditionName != null) {
			strQuery="Select * from "+ sheetName + " where "+ filterConditionName + " = '"  + filetrConditionValue + "'";
		} else {
			strQuery="Select * from "+ sheetName;
		}
		Recordset recordset = connection.executeQuery(strQuery);
		connection.close();
		return recordset;
	}

	//Read Excel Data Values
	public static ArrayList<String> ReadExcelDataValue(String filePath,String columnName,String sheetName,String filterConditionName,String filetrConditionValue) throws FilloException 
	{ 
		ArrayList<String> values = new ArrayList<String>();
		Fillo fillo = new Fillo();
		String strQuery;
		Connection connection = fillo.getConnection(filePath);
		if (filterConditionName != null) {
			strQuery="Select * from "+ sheetName + " where "+ filterConditionName + " = '"  + filetrConditionValue + "'";
		} else {
			strQuery="Select * from "+ sheetName;
		}
		Recordset recordset = connection.executeQuery(strQuery);
		while(recordset.next()) {
			ArrayList<String> dataColl = recordset.getFieldNames();
			Iterator<String> dataIterator = dataColl.iterator();
			while(dataIterator.hasNext()) {
				for (int i = 0; i <= dataColl.size()-1; i++) {
					String data = dataIterator.next();
					String dataVal = recordset.getField(data);
					String testData = dataColl.get(i);
					if (testData.equalsIgnoreCase(columnName)) 
					{
						values.add(recordset.getField(testData));
					}
				}
				break;
			}
		}
		recordset.close();
		connection.close();
		return values;
	}

	private static Workbook getTestExecutionWorkbook() {

		Workbook workbook = null;

		//Create a object of File class to open xlsx file

		try {
			String fileName = "MyQTestExecution_Controller.xlsm";

			File file =    new File(".//TestConfig" + "//" + fileName);

			//Create an object of FileInputStream class to read excel file
			FileInputStream inputStream = new FileInputStream(file);

			//Find the file extension by spliting file name in substring and getting only extension name
			String fileExtensionName = fileName.substring(fileName.indexOf("."));

			//Check condition if the file is xlsx file
			if (fileExtensionName.equals(".xlsx")){

				//If it is xlsx file then create object of XSSFWorkbook class
				workbook = new XSSFWorkbook(inputStream);
			}

			//Check condition if the file is xls file
			else if (fileExtensionName.equals(".xls")){
				//If it is xls file then create object of XSSFWorkbook class
				workbook = new HSSFWorkbook(inputStream);
			}
			//Check condition if the file is xlsm file (micro enabled)
			else if (fileExtensionName.equals(".xlsm")){
				//If it is xls file then create object of XSSFWorkbook class
				workbook = new XSSFWorkbook(inputStream);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return workbook;
	}

	/**
	 * Description : This function would read all the device info parameters from excel file and return all the parameters that are
	 * essential for instantiating android/ios drivers
	 */
	public static String getParameters(String deviceID, int dataCell) {
		String testData = null;
		try {
			if (workbook == null) {
				workbook = UtilityExcel.getTestExecutionWorkbook();
			}
			//Get the DeviceInfo sheet to read the parameters
			Sheet deviceInfoSheet = workbook.getSheet("DeviceInfo");

			//Find number of rows in excel file
			int rowCount = deviceInfoSheet.getLastRowNum()-deviceInfoSheet.getFirstRowNum();

			//Create a loop over all the rows of excel file to read it
			for (int i = 1; i < rowCount + 1; i++) {
				Row row = deviceInfoSheet.getRow(i);
				//Get the device id and read the requested parameter by 'dataCell' number 
				if (row.getCell(0).getStringCellValue().equalsIgnoreCase(deviceID)) {
					testData = row.getCell(dataCell).getStringCellValue();
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return testData;
	}
}