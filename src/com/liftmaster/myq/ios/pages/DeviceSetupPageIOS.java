package com.liftmaster.myq.ios.pages;

import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.liftmaster.myq.utils.UtilityIOS;
import com.sun.javafx.iio.ios.IosDescriptor;

import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;

public class DeviceSetupPageIOS {

	public IOSDriver<IOSElement> iOSDriver;
	private ExtentTest extentTestIOS;
	public UtilityIOS utilityIOS;
	
	//Page factory for Device Setup page IOS
	@iOSFindBy(accessibility = "Ceiling Installed")
	public IOSElement wifigdo_ceilinginstalled;

	@iOSFindBy(accessibility = "We Apologize")
	public IOSElement we_apologize_text;
	
	@iOSFindBy(accessibility = "Smart Garage Hub")
	public IOSElement smart_garage_hub;

	@iOSFindBy(accessibility = "Next")
	public IOSElement next_button;

	@iOSFindBy(accessibility = "Wi-Fi Setup")
	public IOSElement wifi_setup_text;
	
	@iOSFindBy(accessibility = "Beep")
	public IOSElement beep_text;
	
	@iOSFindBy(accessibility = "Yes")
	public IOSElement yes_btn_beep;
	
	@iOSFindBy(accessibility = "DIDN'T HEAR A BEEP >")
	public IOSElement didint_hear_a_beep_btn;
		
	@iOSFindBy(accessibility = "BackBarButtonItem")
	public IOSElement back_btn;
	
	@iOSFindBy(accessibility = "Okay")
	public IOSElement okay_text;
	
	@iOSFindBy(accessibility = "Pull Down")
	public IOSElement pull_down_text;
	
	@iOSFindBy(accessibility = "Reset Wi-Fi")
	public IOSElement reset_wifi_text;
	
	@iOSFindBy(accessibility = "Wi-Fi Mode")
	public IOSElement wifi_mode_text;
	
	@iOSFindBy(accessibility = "Locate Wires")
	public IOSElement locate_wires_text;
	
	@iOSFindBy(accessibility = "Serial Number")
	public IOSElement serial_number_text;
	
	@iOSFindBy(accessibility = "Blue Light")
	public IOSElement blue_light_text;
	
	@iOSFindBy(accessibility = "Device Setup")	
	public IOSElement device_setup_text;
	
	@iOSFindBy(accessibility = "DON'T SEE A BLINKING BLUE LIGHT >")
	public IOSElement dont_see_blue_light_link;
	
	@iOSFindBy(accessibility = "Green Light")
	public IOSElement green_light_text;
	
	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeButton")
	public IOSElement back_btn_error_screen;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[2]")
	public IOSElement dont_see_green_light_link;
	
	@iOSFindBy(accessibility = "Unplug")
	public IOSElement unplug_text;
	
	@iOSFindBy(accessibility = "done_button")
	public IOSElement done_btn;
	
	@iOSFindBy(accessibility = "Retry")
	public IOSElement retry_btn;
	
	@iOSFindBy(accessibility = "DismissBarButtonItem")
	public IOSElement close_btn;
	
	@iOSFindBy(accessibility = "Placeholder for FAQ on second failure")
	public IOSElement second_error_text;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[1]/XCUIElementTypeButton[1]")
	public IOSElement wallcontrol_880lm;	
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[1]/XCUIElementTypeButton[2]")
	public IOSElement wallcontrol_886lm;	
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeButton[1]")
	public IOSElement wallcontrol_883lm;	
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeButton[2]")
	public IOSElement wallcontrol_888lm;	
	
	@iOSFindBy(xpath = "//XCUIElementTypeTextField[@value='1234']")
	public IOSElement serialno_first_textbox;	
	
	@iOSFindBy(xpath = "//XCUIElementTypeTextField[@value='567']")
	public IOSElement serialno_second_textbox;	
	
	@iOSFindBy(xpath = "//XCUIElementTypeTextField[@value='89A']")
	public IOSElement serialno_third_textbox;	

	@iOSFindBy(accessibility="1-800-123-4567")
	public IOSElement customer_service_number;	
	
	@iOSFindBy(accessibility = "The serial number was invalid. Please try again. (301)")
	public IOSElement error_msg_invalid_serialno;
	
	@iOSFindBy(accessibility = "OK")
	public IOSElement error_invalid_serialno_ok_btn;

	@iOSFindBy(accessibility = "Oops")
	public IOSElement oops_text;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@label,\"We cannot add your MyQ device\")]")
	public IOSElement oops_msg_text;
	
	@iOSFindBy(accessibility = "Sorry")
	public IOSElement sorry_text;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@label,\"add your MyQ device\")]")
	public IOSElement sorry_msg_text;
	
	@iOSFindBy(accessibility = "Step_1")
	public IOSElement step1_image;
	                           
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@label,\"Release the button\")]")
	public IOSElement step1_text;
	
	@iOSFindBy(accessibility = "Step_2")
	public IOSElement step2_image;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@label,\"The Smart Garage Hub will reset\")]")
	public IOSElement step2_text;
	
	@iOSFindBy(accessibility = "Plug_in_Hub")
	public IOSElement plug_in_hub_image;
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@label,\"Unplug then replug\")]")
	public IOSElement plug_in_hub_text;	
	
	@iOSFindBy(accessibility = "Try Again")
	public IOSElement try_again_button;	
	
	@iOSFindBy(xpath = "//XCUIElementTypeStaticText[contains(@label,\"1800 123 4567‬\")]")
	public IOSElement customer_label_alert;
	
	@iOSFindBy(accessibility = "Call")
	public IOSElement customer_call_button;

	@iOSFindBy(accessibility = "Cancel")
	public IOSElement customer_cancel_button;
	
	/**
	 * Description:This is the Initializer method
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public DeviceSetupPageIOS(IOSDriver<IOSElement> iOSDriver,ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);			
		this.extentTestIOS = extentTestIOS;
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}
	
	/**
	 * Description : This function will click on the Next button
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickNextButton() {
		try {
			utilityIOS.clickIOS(next_button);
			extentTestIOS.info("Clicked on Next button");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will click on the Try Again button
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickTryAgainButton() {
		try {
			utilityIOS.scroll("down");
			Thread.sleep(1000);
			utilityIOS.clickIOS(try_again_button);
			extentTestIOS.info("Clicked on Try Again button");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will click on customer service number
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickCustomerServiceNumber() {
		try {
			utilityIOS.clickIOS(customer_service_number);
			extentTestIOS.info("Clicked on Customer Service Number link");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will select the wall control
	 * @return Page object
	 */
	public DeviceSetupPageIOS selectWallControl(String wallControl) {
		try {
			if(wallControl.equalsIgnoreCase("wallControl_880lm")){
				utilityIOS.clickIOS(wallcontrol_880lm);
				extentTestIOS.info("Clicked on wall control wall control 880lm ");
			}
			else if(wallControl.equalsIgnoreCase("wallControl_886lm")){
				utilityIOS.clickIOS(wallcontrol_886lm);
				extentTestIOS.info("Clicked on wall control 886lm");
				
			}else if(wallControl.equalsIgnoreCase("wallControl_883lm")){
				utilityIOS.clickIOS(wallcontrol_883lm);
				extentTestIOS.info("Clicked on wall control 883lm");
			}
			else if(wallControl.equalsIgnoreCase("wallControl_888lm")){
				utilityIOS.clickIOS(wallcontrol_888lm);
				extentTestIOS.info("Clicked on wall control 888lm");
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will click on yes button
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickOnYesButton() {
		try {
			utilityIOS.clickIOS(yes_btn_beep);
			extentTestIOS.info("Clicked on yes button on screen");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click the dint hear a beep button
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickonDidnthearBeepButton() {
		try {
			utilityIOS.clickIOS(didint_hear_a_beep_btn);
			extentTestIOS.info("Clicked on didnt hear a beep button on beep screen");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}	
	
	/**
	 * Description : This function will click the next button
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickOnWifiGDO_Ceiling() {
		try {
			utilityIOS.clickIOS(wifigdo_ceilinginstalled);
			extentTestIOS.info("Clicked on wifi gdo on devices setup screen");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will click the smart garage hub device on device setup screen
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickOnSmartGarageHub() {
		try {
			utilityIOS.clickIOS(smart_garage_hub);
			extentTestIOS.info("Clicked on Smart Garage Hub on devices setup screen");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will click the back button
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickOnBackButton() {
		try {
			utilityIOS.waitForElementIOS(back_btn, "visibility");
			utilityIOS.clickIOS(back_btn);
			extentTestIOS.info("Clicked on back button on screen");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will click the back button on error screen
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickOnBackErrorButton() {
		try {
			//utilityIOS.waitForElementIOS(back_btn, "visibility");
			utilityIOS.clickIOS(back_btn_error_screen);
			extentTestIOS.info("Clicked on back button on error failure screen");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will click the dint see blue light link
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickOndDidntSeeBluelightLink() {
		try {
			utilityIOS.clickIOS(dont_see_blue_light_link);
			extentTestIOS.info("Clicked on Dont See Blue light link on screen");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will click the dint see green light link
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickOndDidntSeeGreenlightLink() {
		try {
			utilityIOS.waitForElementIOS(dont_see_green_light_link, "clickable");
			utilityIOS.clickIOS(dont_see_green_light_link);
			extentTestIOS.info("Clicked on Dont See Green light link on screen");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click the back button
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickOnCloseButton() {
		try {
			utilityIOS.clickIOS(close_btn);
			extentTestIOS.info("Clicked on close button on screen");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will click on Retry button
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickOnRetryButton() {
		try {
			utilityIOS.clickIOS(retry_btn);
			extentTestIOS.info("Clicked on retry button on screen");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : this function is to enter serial number
	 * @return Page object
	 */
	public DeviceSetupPageIOS enterSerialNumber(String deviceSerialNumber) {
		try {
			String MyQSrNumber = deviceSerialNumber.substring(2);
			String textBox1 = MyQSrNumber.split(" ")[0].substring(0, 4);
			String textBox2 = MyQSrNumber.split(" ")[0].substring(4, 7);
			String textBox3 = MyQSrNumber.split(" ")[0].substring(7);
			utilityIOS.clickIOS(serialno_first_textbox);
			utilityIOS.enterGWSerialNoInTextBox(textBox1);
			Thread.sleep(500);
			
			utilityIOS.enterGWSerialNoInTextBox(textBox2);
			Thread.sleep(500);
			
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(textBox3), null);
			utilityIOS.clickIOS(serialno_third_textbox);
			utilityIOS.enterGWSerialNoInTextBox(textBox3);	
			Thread.sleep(1000);
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will click the back button
	 * @return Page object
	 */
	public DeviceSetupPageIOS clickOnOKButtonErrorInvalidSerialno() {
		try {
			utilityIOS.clickIOS(error_invalid_serialno_ok_btn);
			extentTestIOS.info("Clicked on OK button on error popup for invalid serial number");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : verify device setup screen is displayed
	 * @return Page object
	 */
	public boolean verifyDeviceSetupTextShown() {
		try {
			utilityIOS.waitForElementIOS(device_setup_text, "visibility");
			if(device_setup_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify Customer Call option is shown
	 * @return Page object
	 */
	public boolean verifyCustomerCallOptionIsShown() {
		try {
			Thread.sleep(1000);
			iOSDriver.switchTo().alert();
			utilityIOS.waitForElementIOS(customer_call_button, "visibility");
			if(customer_call_button.isDisplayed() && customer_cancel_button.isDisplayed()) {
				iOSDriver.switchTo().alert().dismiss();
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify given serial number is entered correctly
	 * @return Page object
	 */
	public boolean verifySerialNumberEntered(String deviceSerialNumber) {
		try {
			String MyQSrNumber = deviceSerialNumber.substring(2);
			String textBox1 = MyQSrNumber.split(" ")[0].substring(0, 4);
			String textBox2 = MyQSrNumber.split(" ")[0].substring(4, 7);
			String textBox3 = MyQSrNumber.split(" ")[0].substring(7);
			if(iOSDriver.findElementByXPath("//XCUIElementTypeTextField[@value='"+textBox1+"']").isDisplayed() && iOSDriver.findElementByXPath("//XCUIElementTypeTextField[@value='"+textBox2+"']").isDisplayed() && iOSDriver.findElementByXPath("//XCUIElementTypeTextField[@value='"+textBox3+"']").isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
		
	/**
	 * Description : verify Oops screen is displayed
	 * @return Page object
	 */
	public boolean verifydOopsScreen() {
		try {
			utilityIOS.waitForElementIOS(oops_text, "visibility");
			if(oops_msg_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify Sorry screen steps are displayed
	 * @return Page object
	 */
	public boolean verifySorryScreenSteps() {
		try {
			utilityIOS.waitForElementIOS(sorry_text, "visibility");
			if(sorry_msg_text.isDisplayed() && step1_image.isDisplayed() && step1_text.isDisplayed() && step2_image.isDisplayed()) 
			{ 	return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify wifi setup screen is displayed
	 * @return Page object
	 */
	public boolean verifyWifiSetupScreen() {
		try {
			utilityIOS.waitForElementIOS(wifi_setup_text, "visibility");
			if(wifi_setup_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify We Apologize screen is displayed
	 * @return Page object
	 */
	public boolean verifyWeApologizeScreen() {
		try {
			utilityIOS.waitForElementIOS(we_apologize_text, "visibility");
			if(we_apologize_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify Beep screen is displayed
	 * @return Page object
	 */
	public boolean verifyBeepScreen() {
		try {
		    utilityIOS.waitForElementIOS(beep_text, "visibility");
			if(beep_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}	
	
	/**
	 * Description : verify Okay screen is displayed
	 * @return Page object
	 */
	public boolean verifyOkayScreen() {
		try {
		    utilityIOS.waitForElementIOS(okay_text, "visibility");
			if(okay_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify Pull down screen is displayed
	 * @return Page object
	 */
	public boolean verifyPullDownScreen() {
		try {
		    utilityIOS.waitForElementIOS(pull_down_text, "visibility");
			if(pull_down_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify Reset Wifi screen is displayed
	 * @return Page object
	 */
	public boolean verifyResetWifiScreen() {
		try {
		    utilityIOS.waitForElementIOS(reset_wifi_text, "visibility");
			if(reset_wifi_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify Wifi mode screen is displayed
	 * @return Page object
	 */
	public boolean verifyWifiModeScreen() {
		try {
		    utilityIOS.waitForElementIOS(wifi_mode_text, "visibility");
			if(wifi_mode_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify locate wires screen is displayed
	 * @return Page object
	 */
	public boolean verifyLocateWiresScreen() {
		try {
		    utilityIOS.waitForElementIOS(locate_wires_text, "visibility");
			if(locate_wires_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**
	 * Description : verify Serial number screen is displayed
	 * @return Page object
	 */
	public boolean verifySerialNumberScreen() {
		try {
		    utilityIOS.waitForElementIOS(serial_number_text, "visibility");
			if(serial_number_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify Second Failure error is displayed
	 * @return Page object
	 */
	public boolean verifySecondFailureError() {
		try {
		    utilityIOS.waitForElementIOS(second_error_text, "visibility");
			if(second_error_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify Blue light screen is displayed
	 * @return Page object
	 */
	public boolean verifyBlueLightScreen() {
		try {
		    utilityIOS.waitForElementIOS(blue_light_text, "visibility");
			if(blue_light_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : verify Green light screen is displayed
	 * @return Page object
	 */
	public boolean verifyGreenLightScreen() {
		try {
		    utilityIOS.waitForElementIOS(green_light_text, "visibility");
			if(green_light_text.isDisplayed()) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
}
