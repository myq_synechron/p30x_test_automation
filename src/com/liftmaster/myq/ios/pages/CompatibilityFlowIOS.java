package com.liftmaster.myq.ios.pages;

import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.liftmaster.myq.utils.UtilityIOS;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class CompatibilityFlowIOS {
	public IOSDriver<IOSElement> iOSDriver;
	private ExtentTest extentTestIOS;
	public UtilityIOS utilityIOS;

	@iOSFindBy(accessibility = "SplashScreenBackgroundPage1")
	public IOSElement backgroundpage1;

	@iOSFindBy(accessibility = "SplashScreenBackgroundPage2")
	public IOSElement backgroundpage2;

	@iOSFindBy(accessibility = "SplashScreenBackgroundPage3")
	public IOSElement backgroundpage3;

	@iOSFindBy(accessibility = "Get Started")
	public IOSElement get_started_btn;

	@iOSFindBy(accessibility = "Buy Now")
	public IOSElement buy_now_btn;

	@iOSFindBy(accessibility = "Smart Garage Hub")
	public IOSElement smart_garage_hub_text;

	@iOSFindBy(accessibility = "Home Bridge")
	public IOSElement home_bridge_text;

	@iOSFindBy(accessibility = "Is my garage door opener compatible? >")
	public IOSElement gdo_compatibility_link;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement brands_list;

	@iOSFindBy(accessibility = "Yes")
	public IOSElement yes_btn;

	@iOSFindBy(accessibility = "No")
	public IOSElement no_btn;

	@iOSFindBy(accessibility = "Sign In")
	public IOSElement sign_in_btn;

	@iOSFindBy(xpath = "//XCUIElementTypePageIndicator")
	public IOSElement splash_screen_page_indicator;

	@iOSFindBy(accessibility = "Select your current garage door opener brand.")
	public IOSElement gdo_selection_text;

	@iOSFindBy(accessibility = "BackBarButtonItem")
	public IOSElement back_btn;

	@iOSFindBy(accessibility = "DismissBarButtonItem")
	public IOSElement exit_btn;

	/**
	 * Description:This is the Initializer method
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public CompatibilityFlowIOS(IOSDriver<IOSElement> iOSDriver,ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);			
		this.extentTestIOS = extentTestIOS;
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}

	/** Description: To click on the GDO compatible link on the home page of compatibility flow
	 * @param userName
	 * @return Page object
	 */
	public CompatibilityFlowIOS clickGdoCompatibleLink() { 
		try {
			utilityIOS.clickIOS(gdo_compatibility_link);
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}

	/**	Description: To click on Yes button on the WiFi GDO and MyQ Logo screen
	 * @return Page object
	 */
	public CompatibilityFlowIOS clickYesBtn( ) { 
		try {
			utilityIOS.clickIOS(yes_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**	Description: To click on No button on the Wi-Fi GDO and MyQ Logo screen
	 * @return Page object
	 */
	public CompatibilityFlowIOS clickNoBtn( ) { 
		try {
			utilityIOS.clickIOS(no_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**	Description: To verify that the get started button is shown on home screen
	 * @return boolean
	 */
	public boolean verifyGetStartedBtn( ) { 
		try {
			utilityIOS.waitForElementIOS(get_started_btn, "Clickable");
			if(get_started_btn.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**	Description:To verify if GDO compatible link is shown on home screen
	 * @return boolean
	 */
	public boolean verifyGDOComptabilelinkshown( ) { 
		try {
			if(gdo_compatibility_link.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**	Description: To click on the sign in button on the home screen
	 * @return boolean
	 */
	public boolean verifySigninBtnShown( ) { 
		try {
			if(sign_in_btn.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**	Description: To verify the visibility and then click on the back button
	 * @return boolean
	 */
	public boolean verifyAndClickBackBtn() { 
		try {
			if(back_btn.isDisplayed()) {
				utilityIOS.clickIOS(back_btn);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**	Description: To verify the visibility and then click on the Exit button
	 * @return boolean
	 */
	public boolean verifyAndClickExitBtn() { 
		try {
			if(back_btn.isDisplayed()) {
				utilityIOS.clickIOS(exit_btn);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**
	 * Description : To select the GDO brand from the list of brands
	 * @param brandName
	 * @throws InterruptedException
	 * @return Page object
	 */
	public CompatibilityFlowIOS selectBrandGDO(String brandName) throws InterruptedException {
		try {
			int noOfBrands = brands_list.findElementsByXPath("XCUIElementTypeCell").size();
			for (int iVal = 1; iVal <= noOfBrands; iVal++) {
				String currentBrand;
				if(iVal==5) {
					currentBrand =  brands_list
							.findElementByXPath("XCUIElementTypeCell[" + iVal + "]/XCUIElementTypeStaticText[1]").getAttribute("name");
				} else {
					currentBrand = brands_list
							.findElementByXPath("XCUIElementTypeCell[" + iVal + "]//./XCUIElementTypeImage[1]").getAttribute("name");
				}
				if (currentBrand.toUpperCase().contains(brandName.toUpperCase())) {
					utilityIOS.clickIOS(brands_list.findElementByXPath("XCUIElementTypeCell[" + iVal + "]"));
					break;
				}
			}
			return this;
		} catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : To verify if all the brands are shown in the compatibility screen
	 * @throws InterruptedException
	 * @return boolean
	 */
	public boolean verifyNumberOfBrands() throws InterruptedException {
		try {
			boolean otherbrandShown = false, liftmasterBrandShown = false, chamberlainBrandShown = false, merlinBrandShown = false, craftsManShown = false;
			int noOfBrands = brands_list.findElementsByXPath("//XCUIElementTypeCell").size();
			for (int iVal = 1; iVal <= noOfBrands; iVal++) { 

				switch(iVal) {
				case 1:
					if(brands_list.findElementByXPath("//XCUIElementTypeCell[" + iVal + "]//./XCUIElementTypeImage[1]").getAttribute("name").toUpperCase().contains("CHAMBERLAIN")) {
						chamberlainBrandShown = true;	
						break;
					} else {
						chamberlainBrandShown = false;	
						utilityIOS.captureScreenshot(iOSDriver);
					}
				case 2:
					if(brands_list.findElementByXPath("//XCUIElementTypeCell[" + iVal + "]//./XCUIElementTypeImage[1]").getAttribute("name").toUpperCase().contains("LIFTMASTER")) {
						liftmasterBrandShown = true;	 
						break;
					}  else {
						liftmasterBrandShown = false;
						utilityIOS.captureScreenshot(iOSDriver);
					}
				case 3:
					if(brands_list.findElementByXPath("//XCUIElementTypeCell[" + iVal + "]//./XCUIElementTypeImage[1]").getAttribute("name").toUpperCase().contains("MERLIN")) {
						merlinBrandShown = true;	 
						break;
					} else {
						merlinBrandShown =  false;
						utilityIOS.captureScreenshot(iOSDriver);
					}
				case 4:
					if(brands_list.findElementByXPath("//XCUIElementTypeCell[" + iVal + "]//./XCUIElementTypeImage[1]").getAttribute("name").toUpperCase().contains("CRAFTSMAN")) {
						craftsManShown = true;
						break;
					} else {
						craftsManShown = false;
						utilityIOS.captureScreenshot(iOSDriver);
					}
				case 5:
					if(brands_list.findElementByXPath("//XCUIElementTypeCell[" + iVal + "]//./XCUIElementTypeStaticText").getAttribute("name").equalsIgnoreCase("Other")) {
						otherbrandShown =true;	
						break;
					} else {
						otherbrandShown = false;	
						utilityIOS.captureScreenshot(iOSDriver);
					}
				}
			}
			if(liftmasterBrandShown && chamberlainBrandShown && merlinBrandShown && craftsManShown && otherbrandShown) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**
	 * Description To verify if all the splash screen is shown
	 * @throws InterruptedException
	 * @return boolean
	 */
	public boolean verifyAllSplashScreen() throws InterruptedException {
		try {
			boolean splashScreenShown1,splashScreenShown2,splashScreenShown3;
			utilityIOS.waitForElementIOS(backgroundpage1, "visibility");
			if(backgroundpage1.isDisplayed()) {
				splashScreenShown1 = true;
			} else {
				splashScreenShown1 = false;
				utilityIOS.captureScreenshot(iOSDriver);
			}
			utilityIOS.waitForElementIOS(backgroundpage2, "visibility");
			if(backgroundpage2.isDisplayed()) {
				splashScreenShown2 = true;
			} else {
				splashScreenShown2 = false;
				utilityIOS.captureScreenshot(iOSDriver);
			}
			utilityIOS.waitForElementIOS(backgroundpage3, "visibility");
			if(backgroundpage3.isDisplayed()) {
				splashScreenShown3 = true;
			} else {
				splashScreenShown3 = false;
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if(splashScreenShown1 && splashScreenShown2 && splashScreenShown3) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**	Description: To verify that the Buy now button is shown
	 * @return Page object
	 */
	public boolean verifyBuyNowBtn( ) { 
		try {
			if(buy_now_btn.isDisplayed()) {
				return true;

			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**	Description: To Verify that smart garage hub text is shown
	 * @return boolean
	 */
	public boolean verifySmartGarageText() { 
		try {
			if(smart_garage_hub_text.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}	

	/**	Description: To verify the Home bridge text is shown
	 * @return boolean
	 */
	public boolean verifyHomeBridgeText() { 
		try {
			if(home_bridge_text.isDisplayed()) {
				return true;

			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}	
}
