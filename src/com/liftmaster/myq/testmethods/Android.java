package com.liftmaster.myq.testmethods;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.liftmaster.myq.android.pages.CompatibilityFlowAndroid;
import com.liftmaster.myq.android.pages.DeviceSetupPageAndroid;
import com.liftmaster.myq.android.pages.LoginPageAndroid;
import com.liftmaster.myq.utils.AppiumServer;
import com.liftmaster.myq.utils.DriverInitializer;
import com.liftmaster.myq.utils.GlobalVariables;
import com.liftmaster.myq.utils.UtilityAndroid;
import com.liftmaster.myq.utils.UtilityExcel;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class Android {

	public static ExtentHtmlReporter extentHtmlReporterAndroid1,extentHtmlReporterAndroid2;
	public static ExtentReports extentReportsAndroid1,extentReportsAndroid2;
	public static ExtentTest extentTestAndroid;
	public AndroidDriver<AndroidElement> androidDriver = null;

	String brandName;
	String platform;
	String deviceName;
	String udid;
	String appPackage;
	String appActivity;
	String appiumServer;
	int appiumPort;
	int bootstrapPort;
	String email;
	String password;
	String wallControl;
	LoginPageAndroid loginPageAndroid = null;
	UtilityAndroid utilityAndroid = null;
	CompatibilityFlowAndroid compatibilityFlowAndroid = null;
	DeviceSetupPageAndroid deviceSetupPageAndroid = null;
	String serialNumber;
	
	String device = null;
	String activeAccountName;
	boolean roleSelected;
	boolean multiUser = true;
	boolean addUserAdmin = false;
	boolean addUserFamily = false;
	boolean addUserGuest = false;
	boolean adminUserAccepted = false;
	boolean familyUserAccepted = false;
	boolean guestUserAccepted = false;

	public String env;
	public String activeUserRole;
	public String userType;
	public int deviceCount;

	public Android(AndroidDriver<AndroidElement> androidDriver,String env,String activeUserRole,String userType){
		this.androidDriver = androidDriver;
		this.env = env;
		this.activeUserRole = activeUserRole;
		this.userType = userType;
	}

	/**
	 * Description	 :	This function is use to create test reports
	 */
	public ExtentTest createTest(String deviceTest, String testName){
		if (deviceTest.equals("Android1")) {
			extentTestAndroid = extentReportsAndroid1.createTest(testName);
		} else {
			extentTestAndroid = extentReportsAndroid2.createTest(testName);
		}
		return extentTestAndroid;
	}

	/**
	 * Description	 :	This function is for configuring reports
	 * @throws IOException 
	 */
	public void configReport(String testName,String environmentName,String deviceTest,String brandName) throws InterruptedException, IOException {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String strDate = formatter.format(date);
		File file = new File(".//Reports//"+strDate);
		if (!file.exists()) {
			file.mkdirs();
		}
		String dateTime = new SimpleDateFormat("dd-MM-yyyy-hhmmss").format(new Date());
		if (deviceTest.equals("Android1")) {
			extentHtmlReporterAndroid1 = new ExtentHtmlReporter(file+"//Reports_"+brandName+"_"+deviceTest+"_"+dateTime+".html");
			Thread.sleep(15000);
			extentReportsAndroid1 = new ExtentReports();		
			extentReportsAndroid1.attachReporter(extentHtmlReporterAndroid1);
			extentReportsAndroid1.setSystemInfo("Environment", environmentName);
			extentHtmlReporterAndroid1.config().setDocumentTitle("MyQ automation test");
			extentHtmlReporterAndroid1.config().setReportName("MyQ Android Report");
			extentHtmlReporterAndroid1.config().setTestViewChartLocation(ChartLocation.TOP);
			extentHtmlReporterAndroid1.config().setTheme(Theme.STANDARD);
		} else {
			extentHtmlReporterAndroid2 = new ExtentHtmlReporter(file+"//Reports_"+brandName+"_"+deviceTest+"_"+dateTime+".html");
			Thread.sleep(15000);
			extentReportsAndroid2 = new ExtentReports();		
			extentReportsAndroid2.attachReporter(extentHtmlReporterAndroid2);
			extentReportsAndroid2.setSystemInfo("Environment", environmentName);
			extentHtmlReporterAndroid2.config().setDocumentTitle("MyQ automation test");
			extentHtmlReporterAndroid2.config().setReportName("MyQ Android Report");
			extentHtmlReporterAndroid2.config().setTestViewChartLocation(ChartLocation.TOP);
			extentHtmlReporterAndroid2.config().setTheme(Theme.STANDARD);
		}
	}

	/**
	 * Description : This function would fetch all the desired capabilities from excel (DeviceInfo sheet)
	 */
	public AndroidDriver<AndroidElement> setUp(String testName) throws InterruptedException {
		brandName = UtilityExcel.getParameters(testName,1);
		platform = UtilityExcel.getParameters(testName,2);
		deviceName  = UtilityExcel.getParameters(testName,3);
		udid = UtilityExcel.getParameters(testName,4);
		appPackage = UtilityExcel.getParameters(testName,5);
		appActivity = UtilityExcel.getParameters(testName,6);
		appiumServer = UtilityExcel.getParameters(testName,7);
		appiumPort = Integer.parseInt(UtilityExcel.getParameters(testName,8));
		bootstrapPort = Integer.parseInt(UtilityExcel.getParameters(testName,9));

		AppiumServer.startServer(appiumServer,appiumPort);
		Thread.sleep(5000);

		androidDriver = DriverInitializer.getAndroidDriver(platform, deviceName, udid, appPackage, appActivity, appiumServer, appiumPort,bootstrapPort);
		return androidDriver;
	}

	/**
	 * Description	 :	This is the logout test case
	 */
	public void logoutAndroid(ArrayList<String> testData, String deviceTest, String scenarioName) {
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid.clickLogout();
	}

	/**
	 * Description : This function would close the driver session
	 */
	public void quitAndroidDriver() {
		androidDriver.quit();
	}

	/**
	 * Description	 :	This function would skip the test cases in Android if, test cases of the groups are not matching with the respective sheet
	 * @param testcase,deviceTest
	 */
	public void skipAndroidTest(String testcase, String deviceTest) {

		extentTestAndroid = createTest(deviceTest,testcase);
		extentTestAndroid.log(Status.SKIP, "Test : " +testcase+ " not found in " +activeUserRole +"_All_Test sheet" );
	}

	/**
	 * Description : This function will start the android driver and login to the application if we get any false failure
	 */
	public void reportFlushAndroid(String deviceTest) throws InterruptedException {
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		if (deviceTest.equals("Android1")) {
			extentReportsAndroid1.flush();
		} else {
			extentReportsAndroid2.flush();
			Thread.sleep(3000);
		}
	}

	/**
	 * Description : This function will ensure that the base condition for a test method is met
	 * @throws InterruptedException 
	 */

	public void baseConditionAndroid(String flowName) throws InterruptedException {
		compatibilityFlowAndroid = new CompatibilityFlowAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		if(flowName.equalsIgnoreCase("Compatibilty")) {
			androidDriver.resetApp();
			loginPageAndroid.selectEnvironment("Pre-Prod")
			.selectVersion("Setup Optimization");
			androidDriver.navigate()
			.back();
			Thread.sleep(2000);
			androidDriver.activateApp(appPackage);
		} else if(flowName.equalsIgnoreCase("Setup")) {
			androidDriver.resetApp();
			loginPageAndroid.selectEnvironment("Pre-Prod")
			.selectVersion("Setup Optimization");
			loginPageAndroid.login(email, password);			
		}
	}

	/**
	 * Description : This function will convert milliseconds to hours, minutes and seconds . This is used for reports
	 * @param millisecond
	 */
	public String msToString(long ms) {
		long totalSecs = ms/1000;
		long hours = (totalSecs / 3600);
		long mins = (totalSecs / 60) % 60;
		long secs = totalSecs % 60;
		String minsString = (mins == 0)
				? "00"
						: ((mins < 10)
								? "0" + mins
										: "" + mins);
		String secsString = (secs == 0)
				? "00"
						: ((secs < 10)
								? "0" + secs
										: "" + secs);
		if (hours > 0)
			return hours + "h:" + minsString + "m:" + secsString+"s";
		else if (mins > 0)
			return mins + "m:" + secsString+"s";
		else return secsString+"s";
	}

	/**
	 * Description : This function will apply the custom CSS onto the extent report for total time
	 */ 
	public void applyCustomCSS(String deviceTest) throws InterruptedException {
		if (deviceTest.equals("Android1")) {
			if (brandName.equalsIgnoreCase("LiftMaster") || brandName.equalsIgnoreCase("Chamberlain"))
			{
				String reqPath = System.getProperty("user.dir")+"//Resources//"+brandName+".jpg";
				long time = extentHtmlReporterAndroid1.getRunDuration();
				String customCSS = "a.brand-logo.blue.darken-3{color: transparent; background: url(file:///"+reqPath+") no-repeat;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div{visibility: hidden;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div:before{content: '"+msToString(time)+"'; visibility: visible; display: block; position: absolute;}";
				extentHtmlReporterAndroid1.config().setCSS(customCSS);
				extentReportsAndroid1.flush();
			} else {
				extentTestAndroid.log(Status.FAIL, brandName +"image is not available in resource folder");
			}
		} else {
			if (brandName.equalsIgnoreCase("LiftMaster") || brandName.equalsIgnoreCase("Chamberlain"))
			{
				String reqPath = System.getProperty("user.dir")+"//Resources//"+brandName+".jpg";
				long time = extentHtmlReporterAndroid2.getRunDuration();
				String customCSS = "a.brand-logo.blue.darken-3{color: transparent; background: url(file:///"+reqPath+") no-repeat;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div{visibility: hidden;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div:before{content: '"+msToString(time)+"'; visibility: visible; display: block; position: absolute;}";
				extentHtmlReporterAndroid2.config().setCSS(customCSS);
				extentReportsAndroid2.flush();
			} else {
				extentTestAndroid.log(Status.FAIL, brandName +"image is not available in resource folder");
			}
		}
	}


	/**
	 * Description	 :	This Android test method verifies the Compatibility flow Home screen, with all 3 rotating images is shown along with Get started button ,GDO compatible link and Sign In button.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void launchingComaptibilityFlowScreenAndroid(ArrayList<String> testData, String deviceTest, String scenarioName) throws InterruptedException {

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		compatibilityFlowAndroid = new CompatibilityFlowAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(!compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			baseConditionAndroid("Compatibility");
		}

		//Verifying all splash screen are shown
		if(compatibilityFlowAndroid.verifyAllSplashScreen()) {
			extentTestAndroid.log(Status.PASS, "All 3 splash screen are shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "All 3 splash screen is not shown");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//Verifying Get started button is shown
		if(compatibilityFlowAndroid.verifyGetStartedBtnOnStartUp()) {
			extentTestAndroid.log(Status.PASS, "Get Started button is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Get started button is not shown");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//Verifying text "Is my GDO Compatible?"
		if(compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			extentTestAndroid.log(Status.PASS, "GDO compatibility link is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "GDO compatibility link is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//Verifying Sign in Button is shown
		if(compatibilityFlowAndroid.verifySigninBtnShown()) {
			extentTestAndroid.log(Status.PASS, "Sign-In button is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Sign-In button is not shown");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//Verifying all the brands are shown on the compatibility screen
		compatibilityFlowAndroid.clickGdoCompatibleLink();
		if(compatibilityFlowAndroid.verifyNumberOfBrands()) {
			extentTestAndroid.log(Status.PASS, "All the Brands are shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "All brands are not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//Clicking back button and verifying that GDO compatibility link is shown after that
		compatibilityFlowAndroid.verifyAndClickBackBtn();
		if(compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			extentTestAndroid.log(Status.PASS, "GDO compatibility link is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "GDO compatibility link is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
	}

	/**
	 * Description	 :	This Android test method verifies the flow of selecting the Liftmaster GDO in the compatibility flow.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyingChamberlainFlow(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		compatibilityFlowAndroid = new CompatibilityFlowAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(!compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			baseConditionAndroid("Compatibility");
		}
		compatibilityFlowAndroid.clickGdoCompatibleLink();
		compatibilityFlowAndroid.selectBrandGDO("Chamberlain")
		.verifyAndClickExitBtn();

		//Clicking on Exit button and verifying that the GDO compatible link is shown on the Home screen
		if(compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			extentTestAndroid.log(Status.PASS, "GDO compatibility link is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "GDO compatibility link is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//Verifying the back button
		compatibilityFlowAndroid.clickGdoCompatibleLink();
		compatibilityFlowAndroid.selectBrandGDO("Chamberlain");
		if(compatibilityFlowAndroid.verifyAndClickBackBtn()) {
			extentTestAndroid.log(Status.PASS, "Back button is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Back button is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//Verifying the yes button flow on the Wifi Screen
		compatibilityFlowAndroid.selectBrandGDO("Chamberlain");
		compatibilityFlowAndroid.clickWiFiYesBtn();

		if(compatibilityFlowAndroid.verifyGetStartedBtnOnCompatibleFlow()){
			extentTestAndroid.log(Status.PASS, "Get started button is shown");			
		} else {
			extentTestAndroid.log(Status.FAIL, "Get started button is not shown");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		compatibilityFlowAndroid.verifyAndClickBackBtn();

		//Verifying the  flow of pressing the No button flow on the Wifi Screen and Yes button on the MyQ screen
		compatibilityFlowAndroid.clickWiFiNoBtn();
		compatibilityFlowAndroid.clickMyqYesBtn();	
		if(compatibilityFlowAndroid.verifyBuyNowBtn() && compatibilityFlowAndroid.verifyHomeBridgeText()){
			extentTestAndroid.log(Status.PASS, "Buy Now button for Home Bridge heading is shown");			
		} else {
			extentTestAndroid.log(Status.FAIL, "Buy Now button for Home Bridge heading is not shown");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//Verifying the  flow of pressing the No button on the MyQ Logo Screen
		compatibilityFlowAndroid.verifyAndClickBackBtn();
		compatibilityFlowAndroid.clickMyqNoBtn();
		if(compatibilityFlowAndroid.verifyBuyNowBtn() && compatibilityFlowAndroid.verifySmartGarageText()){
			extentTestAndroid.log(Status.PASS, "Buy now link for Smart garage hub is shown");			
		} else {
			extentTestAndroid.log(Status.FAIL, "Buy now link for Smart garage hub is not shown");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		compatibilityFlowAndroid.verifyAndClickExitBtn();	
	}


	/**
	 * Description	 :	This Android test method verifies the flow of selecting the Craftsman GDO in the compatibility flow.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyingLiftMasterFlow(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		compatibilityFlowAndroid = new CompatibilityFlowAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);


		if(!compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			baseConditionAndroid("Compatibility");
		}
		
		//Click on GDO compatibility link
		compatibilityFlowAndroid.clickGdoCompatibleLink();
		compatibilityFlowAndroid.selectBrandGDO("Liftmaster")
		.verifyAndClickExitBtn();
		
		//Verifying GDO Compatibility link is shown
		if(compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			extentTestAndroid.log(Status.PASS, "GDO compatibility link is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "GDO compatibility link is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		compatibilityFlowAndroid.clickGdoCompatibleLink();
		compatibilityFlowAndroid.selectBrandGDO("Liftmaster");
		
		//Verifying back button is shown
		if(compatibilityFlowAndroid.verifyAndClickBackBtn()) {
			extentTestAndroid.log(Status.PASS, "back button is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "back button is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		compatibilityFlowAndroid.selectBrandGDO("Liftmaster");
		compatibilityFlowAndroid.clickWiFiYesBtn();

		//Verifying Get Started button is shown
		if(compatibilityFlowAndroid.verifyGetStartedBtnOnCompatibleFlow()){
			extentTestAndroid.log(Status.PASS, "Get started button is shown");			
		} else {
			extentTestAndroid.log(Status.PASS, "Get started button is not shown");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		compatibilityFlowAndroid.verifyAndClickBackBtn();
		compatibilityFlowAndroid.clickWiFiNoBtn();
		compatibilityFlowAndroid.clickMyqYesBtn();	
		
		//Verifying Buy Now button for Home Bridge is shown
		if(compatibilityFlowAndroid.verifyBuyNowBtn() && compatibilityFlowAndroid.verifyHomeBridgeText()){
			extentTestAndroid.log(Status.PASS, "Buy Now button for Home Bridge heading is shown");			
		} else {
			extentTestAndroid.log(Status.PASS, "Buy Now button for Home Bridge heading is not shown");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		compatibilityFlowAndroid.verifyAndClickBackBtn();
		compatibilityFlowAndroid.clickMyqNoBtn();
		
		//Verifying Buy now link for smart garage hub is shown
		if(compatibilityFlowAndroid.verifyBuyNowBtn() && compatibilityFlowAndroid.verifySmartGarageText()){
			extentTestAndroid.log(Status.PASS, "Buy now link for Smart garage hub is shown");			
		} else {
			extentTestAndroid.log(Status.PASS, "Buy now link for Smart garage hub is not shown");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		compatibilityFlowAndroid.verifyAndClickExitBtn();	
	}


	/**
	 * Description	 :	This Android test method verifies the flow of selecting the Craftsman GDO in the compatibility flow.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyingCraftsmanFlow(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		compatibilityFlowAndroid = new CompatibilityFlowAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(!compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			baseConditionAndroid("Compatibility");
		}
		compatibilityFlowAndroid.clickGdoCompatibleLink();
		compatibilityFlowAndroid.selectBrandGDO("Craftsman")
		.verifyAndClickExitBtn();
		
		// Verifying GDO compatibility link is shown
		if(compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			extentTestAndroid.log(Status.PASS, "GDO compatibility link is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "GDO compatibility link is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		compatibilityFlowAndroid.clickGdoCompatibleLink();
		compatibilityFlowAndroid.selectBrandGDO("Craftsman");
		
		//Verifying Back button is shown
		if(compatibilityFlowAndroid.verifyAndClickBackBtn()) {
			extentTestAndroid.log(Status.PASS, "back button is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "back button is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		
		//Selecting craftsman GDO
		compatibilityFlowAndroid.selectBrandGDO("Craftsman");
		compatibilityFlowAndroid.clickWiFiYesBtn();

		//Verifying Get Started Button is shown
		if(compatibilityFlowAndroid.verifyGetStartedBtnOnCompatibleFlow()){
			extentTestAndroid.log(Status.PASS, "Get started button is shown");			
		} else {
			extentTestAndroid.log(Status.PASS, "Get started button is not shown");
		}
		compatibilityFlowAndroid.verifyAndClickBackBtn();
		compatibilityFlowAndroid.clickWiFiNoBtn();
		
		//Verifying Buy now link for Smart garage hub is shown
		if(compatibilityFlowAndroid.verifyBuyNowBtn() && compatibilityFlowAndroid.verifySmartGarageText()){
			extentTestAndroid.log(Status.PASS, "Buy now link for Smart garage hub is shown");			
		} else {
			extentTestAndroid.log(Status.PASS, "Get started button is not shown");
		}
		compatibilityFlowAndroid.verifyAndClickExitBtn();
	}

	/**
	 * Description	 :	This Android test method verifies the flow of selecting the Merlin GDO in the compatibility flow.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyingMerlinFlow(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		compatibilityFlowAndroid = new CompatibilityFlowAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(!compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			baseConditionAndroid("Compatibility");
		}
		
		compatibilityFlowAndroid.clickGdoCompatibleLink();
		//Select Brand Merlin
		compatibilityFlowAndroid.selectBrandGDO("Merlin")
		.verifyAndClickExitBtn();

		//Verifying GDO Compatibility link is shown
		if(compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			extentTestAndroid.log(Status.PASS, "GDO compatibility link is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "GDO compatibility link is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}

		compatibilityFlowAndroid.clickGdoCompatibleLink();
		compatibilityFlowAndroid.selectBrandGDO("Merlin");
		
		//Verifying Back button is shown
		if(compatibilityFlowAndroid.verifyAndClickBackBtn()) {
			extentTestAndroid.log(Status.PASS, "back button is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "back button is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		
		compatibilityFlowAndroid.selectBrandGDO("Merlin");
		
		//Verifying Get Started Button is shown
		if(compatibilityFlowAndroid.verifyGetStartedBtnOnCompatibleFlow()){
			extentTestAndroid.log(Status.PASS, "Get started button is shown");			
		} else {
			extentTestAndroid.log(Status.PASS, "Get started button is not shown");
		}
		compatibilityFlowAndroid.verifyAndClickExitBtn();
	}


	/**
	 * Description	 :	This Android test method verifies the flow of selecting the Other Brand GDO in the compatibility flow.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyingOtherFlow(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		compatibilityFlowAndroid = new CompatibilityFlowAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(!compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			baseConditionAndroid("Compatibility");
		}
		
		compatibilityFlowAndroid.clickGdoCompatibleLink();
		compatibilityFlowAndroid.selectBrandGDO("Other")
		.verifyAndClickExitBtn();

		//Verifying GDO compatibility link is shown
		if(compatibilityFlowAndroid.verifyGDOComptabilelinkShown()) {
			extentTestAndroid.log(Status.PASS, "GDO compatibility link is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "GDO compatibility link is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		compatibilityFlowAndroid.clickGdoCompatibleLink();
		compatibilityFlowAndroid.selectBrandGDO("Other");
		
		//Verifying back button is shown
		if(compatibilityFlowAndroid.verifyAndClickBackBtn()) {
			extentTestAndroid.log(Status.PASS, "Back button is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Back button is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		compatibilityFlowAndroid.selectBrandGDO("Other");
		compatibilityFlowAndroid.verifyAndClickExitBtn();
	}

	/**
	 * Description	:	This is the login test case
	 * @throws InterruptedException 
	 */
	public void loginAndroid(ArrayList<String> testData, String deviceTest, String scenarioValue) throws InterruptedException {
		email = testData.get(0).split(",")[0];
		password = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest,scenarioValue); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		deviceSetupPageAndroid = new DeviceSetupPageAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid.selectEnvironment(env)
		.selectVersion(userType)
		.login(email, password);
		deviceSetupPageAndroid.clickOkLocationButton();
		deviceSetupPageAndroid.clickAllowButton();
		deviceSetupPageAndroid.waitWhileSearchingDevices();
	}

	/**
	 * Description	 :	This is the test method for Verifying the flow of Wifi Provisioning GDO error scenarios 
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyingProvisioningWGDO(ArrayList<String> testData, String deviceTest, String scenarioValue) throws InterruptedException {

		extentTestAndroid = createTest(deviceTest,scenarioValue); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		compatibilityFlowAndroid = new CompatibilityFlowAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		deviceSetupPageAndroid = new DeviceSetupPageAndroid(androidDriver, extentTestAndroid);
		wallControl=testData.get(0).split(",")[0];
         
 		if(!deviceSetupPageAndroid.verifyDevicesSetupScreen()) {
			baseConditionAndroid("Setup");
		}
        
         //Select WifiGDO on device setup page and click on Next button on what you need page
		deviceSetupPageAndroid.selectWifiGDO();
		deviceSetupPageAndroid.clickNextButtonOnWhatYouNeed()
		.select880LmWallControl(wallControl)
		.clickNextButtonOnWallControl();
	    
		//Verify that Beep screen is shown and click on Yes button then wait for devices to load
		if(deviceSetupPageAndroid.verifyBeepScreenShown()) {
			extentTestAndroid.log(Status.PASS, "Beep Screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Beep Screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickYesButtonOnBeepScreen();
		deviceSetupPageAndroid.waitWhileLoadingDevices();
	
		//Verifying the Error Screen 1 is shown and then click on Next button 
		if(deviceSetupPageAndroid.verifyWgdoErrorScreen1()) {
			extentTestAndroid.log(Status.PASS, "Error Screen 1 is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Error Screen 1 is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickNextButtonErrorScreens();

		//Verifying the Locate Wires WGDO screen is shown and click on next button
		if(deviceSetupPageAndroid.verifyLocateWiresWGDOScreen()) {
			extentTestAndroid.log(Status.PASS, "locate wires screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "locate wires screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickNextButtonErrorScreens();

		// Verifying the Pull down WGDO screen and click on next button
		if(deviceSetupPageAndroid.verifyPullDownWgdoScreen()) {
			extentTestAndroid.log(Status.PASS, "Pull down the light cover screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Pull down the light cover screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickNextButtonErrorScreens(); 
		
		// Verifying the Reset Wifi WGDO screen is shown and click on next button
		if(deviceSetupPageAndroid.verifyResetWiFiWgdoScreen()) {
			extentTestAndroid.log(Status.PASS, "Reset WiFi wgdo screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Reset WiFi wgdo screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickNextButtonErrorScreens();

		//Verifying the Wi-Fi mode error screen is shown and click on next button
		if(deviceSetupPageAndroid.verifyWifiModeWgdoScreen()) {
			extentTestAndroid.log(Status.PASS, "Wi-FI mode error screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Wi-Fi mode screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickNextButtonErrorScreens();
		
		//Verifying the Beep screen is shown and click on Yes button
		if(deviceSetupPageAndroid.verifyBeepScreenShown()) {
			extentTestAndroid.log(Status.PASS, "Beep Screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Beep Screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickYesButtonOnBeepScreen();
		deviceSetupPageAndroid.waitWhileSearchingDevices();
		
		// Verifying the FAQ screen is shown and click on exit link
		if(deviceSetupPageAndroid.verifyFAQScreen()) {
			extentTestAndroid.log(Status.PASS, "FAQ Screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "FAQ Screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.exitSetupFlow();
		
		// Select WiFi GDO on device setup page and click on next button, then select wall control
		deviceSetupPageAndroid.selectWifiGDO();
		deviceSetupPageAndroid.clickNextButtonOnWhatYouNeed()
		.select880LmWallControl(wallControl)
		.clickNextButtonOnWallControl();
	    
		//Verify that Beep screen is shown and click on dint hear a beep link then wait for devices to load
		if(deviceSetupPageAndroid.verifyBeepScreenShown()) {
			extentTestAndroid.log(Status.PASS, "Beep Screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Beep Screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickDidntHearABeepLink();
	
		//Verifying the Error Screen 1 is shown and then click on Next button 
		if(deviceSetupPageAndroid.verifyWgdoErrorScreen1()) {
			extentTestAndroid.log(Status.PASS, "Error Screen 1 is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Error Screen 1 is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickNextButtonErrorScreens();

		//Verifying the Locate Wires WGDO screen is shown and click on next button
		if(deviceSetupPageAndroid.verifyLocateWiresWGDOScreen()) {
			extentTestAndroid.log(Status.PASS, "locate wires screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "locate wires screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickNextButtonErrorScreens();

		// Verifying the Pull down WGDO screen and click on next button
		if(deviceSetupPageAndroid.verifyPullDownWgdoScreen()) {
			extentTestAndroid.log(Status.PASS, "Pull down the light cover screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Pull down the light cover screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickNextButtonErrorScreens();
		
		// Verifying the Reset Wifi WGDO screen is shown and click on next button
		if(deviceSetupPageAndroid.verifyResetWiFiWgdoScreen()) {
			extentTestAndroid.log(Status.PASS, "Reset WiFi wgdo screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Reset WiFi wgdo screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickNextButtonErrorScreens();

		//Verifying the Wi-Fi mode error screen is shown and click on next button
		if(deviceSetupPageAndroid.verifyWifiModeWgdoScreen()) {
			extentTestAndroid.log(Status.PASS, "Wi-FI mode error screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Wi-Fi mode screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickNextButtonErrorScreens();
		
		//Verifying the Beep screen is shown and click on Dint hear a beep link
		if(deviceSetupPageAndroid.verifyBeepScreenShown()) {
			extentTestAndroid.log(Status.PASS, "Beep Screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Beep Screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.clickDidntHearABeepLink();
		deviceSetupPageAndroid.waitWhileSearchingDevices();
		
		// Verifying FAQ screen is shown and click on exit link 
		if(deviceSetupPageAndroid.verifyFAQScreen()) {
			extentTestAndroid.log(Status.PASS, "FAQ Screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "FAQ Screen is not shown");	
			utilityAndroid.captureScreenshot(androidDriver);
		}
		deviceSetupPageAndroid.exitSetupFlow();
	}

	/**
	 * Description	 :	Verifying the flow of SmartHub Provisioning Error scenarios
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyingProvisioningSmartHub(ArrayList<String> testData, String deviceTest,String scenarioValue) throws InterruptedException {

		extentTestAndroid = createTest(deviceTest,scenarioValue); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		compatibilityFlowAndroid = new CompatibilityFlowAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		deviceSetupPageAndroid = new DeviceSetupPageAndroid(androidDriver, extentTestAndroid);
	
		serialNumber=testData.get(0).split(",")[0];	 
 		if(!deviceSetupPageAndroid.verifyDevicesSetupScreen()) {
			baseConditionAndroid("Setup");
		}
 		
		deviceSetupPageAndroid.selectSmartHub();
		deviceSetupPageAndroid.clickNextButtonOnWhatYouNeed()
		.clickNextBtnOnPowerUp()
		.waitWhileLoadingDevices();
		
		// Verifying Sorry screen (Error screen 1) is shown and click on OK button
		if(deviceSetupPageAndroid.verifyDiscoveryDevicesSorryScreen()) {
			extentTestAndroid.log(Status.PASS, "Error Screen 1 is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Error Screen 1 is not shown");		
		}
		deviceSetupPageAndroid.clickOkButtonOnSorryPage();
		deviceSetupPageAndroid.clickDontSeeGreenLightLink()
		.clickUnplugRetryButton()
		.waitWhileLoadingDevices();
		
		// Verifying the Sorry screen is shown and click on Try Again button
		if(deviceSetupPageAndroid.verifyUnableToAddSorryPage()) {
			extentTestAndroid.log(Status.PASS, "Sorry Screen is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Sorry Screen is not shown");		
		}
		deviceSetupPageAndroid.clickTryAgain()
		.waitWhileLoadingDevices();
		
		// Verifying the Apologize page is shown
		if(deviceSetupPageAndroid.verifyApologizePage()) {
			extentTestAndroid.log(Status.PASS, "Apologize page is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Apologize page is not shown");		
		}
		deviceSetupPageAndroid.exitSetupFlow();
		
		// Flow with clicking on Yes button on Green light screen
		deviceSetupPageAndroid.selectSmartHub();
		deviceSetupPageAndroid.clickNextButtonOnWhatYouNeed()
		.clickNextBtnOnPowerUp()
		.waitWhileLoadingDevices()
		.clickOkButtonOnSorryPage();
		deviceSetupPageAndroid.clickYesGreenLightSmartGarageHub();
		
		// Verifying Serial number page is shown
		if(deviceSetupPageAndroid.verifySerialNumberPage()) {
			extentTestAndroid.log(Status.PASS, "Serial number page is shown");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Serial number page is not shown");		
		}
		//Enter serial number 
		deviceSetupPageAndroid.enterSerialNumber(serialNumber);
		//Verifying entered serial number
		if(deviceSetupPageAndroid.verifySerialNumberEntered(serialNumber)) {
			extentTestAndroid.log(Status.PASS, "Serial number is entered successfully");		
		} else {
			extentTestAndroid.log(Status.FAIL, "Serial number is not entered successfully");		
		}
		deviceSetupPageAndroid.exitSetupFlow();
	}
}