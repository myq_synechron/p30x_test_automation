
package com.liftmaster.myq.testmethods;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.liftmaster.myq.ios.pages.CompatibilityFlowIOS;
import com.liftmaster.myq.ios.pages.DeviceSetupPageIOS;
import com.liftmaster.myq.ios.pages.LoginPageIOS;
import com.liftmaster.myq.utils.AppiumServer;
import com.liftmaster.myq.utils.DriverInitializer;
import com.liftmaster.myq.utils.GlobalVariables;
import com.liftmaster.myq.utils.UtilityIOS;
import com.liftmaster.myq.utils.UtilityExcel;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;

public class IOS {

	public static ExtentHtmlReporter extentHtmlReporterIOS1,extentHtmlReporterIOS2;
	public static ExtentReports extentReportsIOS1,extentReportsIOS2;
	public static ExtentTest extentTestIOS;

	public static String brandName;
	String platform;
	String deviceName;
	String udid;
	String appiumServer;
	String bundleId;
	String automationName;
	String xcodeOrgId;
	String xcodeSigningId;
	int appiumPort;
	String deviceSerialNumber;

	IOSDriver<IOSElement> iOSDriver = null;
	LoginPageIOS loginPageIOS = null;
	UtilityIOS utilityIOS = null;
	CompatibilityFlowIOS compatibilityFlowIOS = null;
	DeviceSetupPageIOS deviceSetupPageIOS = null;
	String device = null;
	boolean multiUser = false;
	boolean expectedRolePresent = false;
	boolean adminInvitationSent = false;
	boolean familyInvitationSent = false;
	boolean guestInvitationSent = false;

	public String env;
	public String activeUserRole;
	public String userType;
	String userName;
	String password;
	String activeAccountName;	

	public IOS(IOSDriver<IOSElement> iOSDriver,String env,String activeUserRole,String userType) {
		this.iOSDriver = iOSDriver;
		this.env = env;
		this.activeUserRole = activeUserRole;
		this.userType = userType;
	}

	/**
	 * Description	 :	This function is use to create test reports
	 */
	public ExtentTest createTest(String deviceTest, String testName)
	{
		if (deviceTest.equals("iOS1")) {
			extentTestIOS = extentReportsIOS1.createTest(testName);
		} else {
			extentTestIOS = extentReportsIOS2.createTest(testName);
		}
		return extentTestIOS;
	}

	/**
	 * Description : This function would fetch all the desired capabilities from excel (DeviceInfo sheet)
	 */
	public IOSDriver<IOSElement> setUp(String testName) throws InterruptedException {

		brandName = UtilityExcel.getParameters(testName,1);
		platform = UtilityExcel.getParameters(testName,2);
		deviceName  = UtilityExcel.getParameters(testName,3);
		udid = UtilityExcel.getParameters(testName,4);
		appiumServer = UtilityExcel.getParameters(testName,7);
		appiumPort = Integer.parseInt(UtilityExcel.getParameters(testName,8));

		//bootstrapPort not required for IOS
		//bootstrapPort = Integer.parseInt(UtilityExcel.getParameters(testName,9));//

		bundleId = UtilityExcel.getParameters(testName,10);
		automationName = UtilityExcel.getParameters(testName,11);
		xcodeOrgId = UtilityExcel.getParameters(testName,12);
		xcodeSigningId = UtilityExcel.getParameters(testName,13);

		AppiumServer.startServer(appiumServer,appiumPort);

		iOSDriver = DriverInitializer.getiOSDriver(platform,deviceName,udid,appiumServer,appiumPort,bundleId,automationName,xcodeOrgId,xcodeSigningId);

		return iOSDriver;
	}

	/**
	 * Description	 :	This is the login test case
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void loginIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		iOSDriver.resetApp();
		userName = testData.get(0).split(",")[0];
		password = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		
		//Selecting the Environment and login
		loginPageIOS.selectEnvironment(env)
		.selectUserType(userType)
		//.selectFeatureFlags("setup_optimization")
		.login(userName, password);
	}

	/**
	 * Description	 :	This function is for configuring reports
	 * @throws IOException 
	 */
	public void configReport(String testName,String environmentName,String deviceTest,String brandName) throws InterruptedException
	{
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String strDate = formatter.format(date);
		File file = new File(".//Reports//"+strDate);
		if (!file.exists()) {
			file.mkdirs();
		}
		String dateTime = new SimpleDateFormat("dd-MM-yyyy-hhmmss").format(new Date());
		if (deviceTest.equals("iOS1"))
		{
			extentHtmlReporterIOS1 = new ExtentHtmlReporter(file+"//Reports_"+brandName+"_"+deviceTest+"_"+dateTime+".html");
			extentReportsIOS1 = new ExtentReports();		
			extentReportsIOS1.attachReporter(extentHtmlReporterIOS1);
			extentReportsIOS1.setSystemInfo("Environment",environmentName);
			extentHtmlReporterIOS1.config().setDocumentTitle("MyQ automation test");
			extentHtmlReporterIOS1.config().setReportName("MyQ iOS Report");
			extentHtmlReporterIOS1.config().setTestViewChartLocation(ChartLocation.TOP);
			extentHtmlReporterIOS1.config().setTheme(Theme.STANDARD);
		} else {
			extentHtmlReporterIOS2 = new ExtentHtmlReporter(file+"//Reports_"+brandName+"_"+deviceTest+"_"+dateTime+".html");
			extentReportsIOS2 = new ExtentReports();		
			extentReportsIOS2.attachReporter(extentHtmlReporterIOS2);
			extentReportsIOS2.setSystemInfo("Environment",environmentName);
			extentHtmlReporterIOS2.config().setDocumentTitle("MyQ automation test");
			extentHtmlReporterIOS2.config().setReportName("MyQ iOS Report");
			extentHtmlReporterIOS2.config().setTestViewChartLocation(ChartLocation.TOP);
			extentHtmlReporterIOS2.config().setTheme(Theme.STANDARD);
		}
	}

	/**
	 * Description : This function would close the driver session
	 */
	public void quitIOSDriver() {
		iOSDriver.quit();
	}

	/**
	 * Description	 :	This function would skip the test cases in IOS, if test cases of the groups are not matching with the respective sheet
	 * @param testcase,deviceTest
	 */
	public void skipIOSTest(String testcase, String deviceTest) {

		extentTestIOS = createTest(deviceTest,testcase);
		extentTestIOS.log(Status.SKIP, "Test : " +testcase+ " not found in " +activeUserRole +"_All_Test sheet" );
	}

	/**
	 * Description : This function will start the iOS driver and login to the application if we get any false failure
	 */
	public void reportFlushIOS(String deviceTest) {
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		if (deviceTest.equals("iOS1")) {
			extentReportsIOS1.flush();
		} else {
			extentReportsIOS2.flush();
		}
	}

	/**
	 * Description : This function will ensure that the base condition for a test method is met
	 * @throws InterruptedException 
	 */

	public void baseConditionIOS(String flowName) throws InterruptedException {
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		compatibilityFlowIOS = new  CompatibilityFlowIOS (iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		if(flowName.equalsIgnoreCase("Compatibility")) {
			iOSDriver.resetApp();
			loginPageIOS.clickP30x();
		} else if(flowName.equalsIgnoreCase("Setup")) {
			iOSDriver.resetApp();
			//Selecting the Environment and login
			loginPageIOS.selectEnvironment(env)
			.selectUserType(userType)
			.selectFeatureFlags("setup_optimization")
			.login(userName, password);		
		}
	}

	/**
	 * Description : This function will convert milliseconds to hours, minutes and seconds
	 */
	public String msToString(long ms) {
		long totalSecs = ms/1000;
		long hours = (totalSecs / 3600);
		long mins = (totalSecs / 60) % 60;
		long secs = totalSecs % 60;
		String minsString = (mins == 0)
				? "00"
						: ((mins < 10)
								? "0" + mins
										: "" + mins);
		String secsString = (secs == 0)
				? "00"
						: ((secs < 10)
								? "0" + secs
										: "" + secs);
		if (hours > 0)
			return hours + "h:" + minsString + "m:" + secsString+"s";
		else if (mins > 0)
			return mins + "m:" + secsString+"s";
		else return secsString+"s";
	}

	/**
	 * Description : This function will apply the custom CSS onto the extent report for total time
	 */
	public void applyCustomCSS(String deviceTest) throws InterruptedException {
		if (deviceTest.equals("iOS1")) {
			if (brandName.equalsIgnoreCase("LiftMaster") || brandName.equalsIgnoreCase("Chamberlain"))
			{
				String reqPath = System.getProperty("user.dir")+"//Resources//"+brandName+".jpg";
				long time = extentHtmlReporterIOS1.getRunDuration();
				String customCSS = "a.brand-logo.blue.darken-3{color: transparent; background: url(file:///"+reqPath+") no-repeat;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div{visibility: hidden;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div:before{content: '"+msToString(time)+"'; visibility: visible; display: block; position: absolute;}";
				extentHtmlReporterIOS1.config().setCSS(customCSS);
				extentReportsIOS1.flush();
			} else {
				extentTestIOS.log(Status.FAIL, brandName +"image is not available in resource folder");
			}
		} else {
			if (brandName.equalsIgnoreCase("LiftMaster") || brandName.equalsIgnoreCase("Chamberlain")) {
				String reqPath = System.getProperty("user.dir")+"//Resources//"+brandName+".jpg";
				long time = extentHtmlReporterIOS2.getRunDuration();
				String customCSS = "a.brand-logo.blue.darken-3{color: transparent; background: url(file:///"+reqPath+") no-repeat;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div{visibility: hidden;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div:before{content: '"+msToString(time)+"'; visibility: visible; display: block; position: absolute;}";
				extentHtmlReporterIOS2.config().setCSS(customCSS);
				extentReportsIOS2.flush();
			} else {
				extentTestIOS.log(Status.FAIL, brandName +"image is not available in resource folder");
			}
		}
	}

	/**
	 * Description	 :	This iOS test method verifies the Compatibility flow Home screen, with all 3 rotating images is shown along with Get started button ,GDO compatible link and Sign In button.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void LaunchingComaptibilityFlowScreenIOS(ArrayList<String> testData, String deviceTest, String scenarioName) throws InterruptedException {

		extentTestIOS = createTest(deviceTest,scenarioName);

		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		compatibilityFlowIOS = new  CompatibilityFlowIOS (iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		loginPageIOS.clickP30x();
		//Verifying all splash screen are shown
		if(compatibilityFlowIOS.verifyAllSplashScreen()) {
			extentTestIOS.log(Status.PASS, "All 3 splash screen is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "All 3 splash screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		//Verifying Get started button is shown
		if(compatibilityFlowIOS.verifyGetStartedBtn()) {
			extentTestIOS.log(Status.PASS, "Get Started button is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "Get started button is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}

		//Verifying "Is my GDO Compatible?"
		if(compatibilityFlowIOS.verifyGDOComptabilelinkshown()) {
			extentTestIOS.log(Status.PASS, "GDO compatible link is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "GDO compatible link is not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}

		//Verifying Sign in Button is shown
		if(compatibilityFlowIOS.verifySigninBtnShown()) {
			extentTestIOS.log(Status.PASS, "Sign-In button is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "Sign-In button is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}

		//Verifying all the brands are shown on the compatibility screen
		compatibilityFlowIOS.clickGdoCompatibleLink();
		if(compatibilityFlowIOS.verifyNumberOfBrands()) {
			extentTestIOS.log(Status.PASS, "All the Brands are shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "All brands are not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}

		//Clicking back button and verifying that GDO compatibility link is shown after that
		compatibilityFlowIOS.verifyAndClickBackBtn();
		if(compatibilityFlowIOS.verifyGDOComptabilelinkshown()) {
			extentTestIOS.log(Status.PASS, "GDO compatibility link is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "GDO compatibility link is not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}
	}

	/**
	 * Description	 :	This iOS test method verifies the flow of selecting the Chamberlain GDO in the compatibility flow.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyingChamberlainFlowIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestIOS = createTest(deviceTest,scenarioName);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		compatibilityFlowIOS = new  CompatibilityFlowIOS (iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		if(!compatibilityFlowIOS.verifyGDOComptabilelinkshown()) {
			baseConditionIOS("Compatibility");
		}
		compatibilityFlowIOS.clickGdoCompatibleLink();
		compatibilityFlowIOS.selectBrandGDO("Chamberlain")
		.verifyAndClickExitBtn();
		if(compatibilityFlowIOS.verifyGDOComptabilelinkshown()) {
			extentTestIOS.log(Status.PASS, "GDO compatible link is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "GDO compatible link is not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}

		//Verifying the back button
		compatibilityFlowIOS.clickGdoCompatibleLink();
		compatibilityFlowIOS.selectBrandGDO("Chamberlain");
		if(compatibilityFlowIOS.verifyAndClickBackBtn()) {
			extentTestIOS.log(Status.PASS, "Back button is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "Back button is not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}

		//Verifying the yes button flow on the Wifi Screen
		compatibilityFlowIOS.selectBrandGDO("Chamberlain");
		compatibilityFlowIOS.clickYesBtn();
		if(compatibilityFlowIOS.verifyGetStartedBtn()){
			extentTestIOS.log(Status.PASS, "Get started button is shown");			
		} else {
			extentTestIOS.log(Status.FAIL, "Get started button is not shown");
		}

		//Verifying the  flow of pressing the No button flow on the Wifi Screen and Yes button on the MyQ screen
		compatibilityFlowIOS.verifyAndClickBackBtn();
		compatibilityFlowIOS.clickNoBtn();
		compatibilityFlowIOS.clickYesBtn();	

		if(compatibilityFlowIOS.verifyBuyNowBtn() && compatibilityFlowIOS.verifyHomeBridgeText()){
			extentTestIOS.log(Status.PASS, "Buy Now button for Home Bridge is shown");			
		} else {
			extentTestIOS.log(Status.FAIL, "Buy Now button for Home Bridge is not shown");
		}

		//Verifying the  flow of pressing the No button on the MyQ Logo Screen
		compatibilityFlowIOS.verifyAndClickBackBtn();
		compatibilityFlowIOS.clickNoBtn();
		if(compatibilityFlowIOS.verifyBuyNowBtn() && compatibilityFlowIOS.verifySmartGarageText()){
			extentTestIOS.log(Status.PASS, "Buy now link for smart garage hub is shown");			
		} else {
			extentTestIOS.log(Status.FAIL, "Buy now link for smart garage hub is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}

		compatibilityFlowIOS.verifyAndClickExitBtn();	
	}

	/**
	 * Description	 :	This iOS test method verifies the flow of selecting the Liftmaster GDO in the compatibility flow.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyingLiftmasterFlowIOS(ArrayList<String> testData, String deviceTest, String scenarioName) throws InterruptedException {

		extentTestIOS = createTest(deviceTest,scenarioName);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		compatibilityFlowIOS = new  CompatibilityFlowIOS (iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		if(!compatibilityFlowIOS.verifyGDOComptabilelinkshown()) {
			baseConditionIOS("Compatibility");
		}

		compatibilityFlowIOS.clickGdoCompatibleLink();
		compatibilityFlowIOS.selectBrandGDO("Liftmaster")
		.verifyAndClickExitBtn();

		if(compatibilityFlowIOS.verifyGDOComptabilelinkshown()) {
			extentTestIOS.log(Status.PASS, "GDO compatible link is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "GDO compatible link is not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}
		
		//Verifying the back button
		compatibilityFlowIOS.clickGdoCompatibleLink();
		compatibilityFlowIOS.selectBrandGDO("Liftmaster");
		if(compatibilityFlowIOS.verifyAndClickBackBtn()) {
			extentTestIOS.log(Status.PASS, "Back button is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "Back button is not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}
		
		//Verifying the yes button flow on the Wifi Screen
		compatibilityFlowIOS.selectBrandGDO("Liftmaster");
		compatibilityFlowIOS.clickYesBtn();
		if(compatibilityFlowIOS.verifyGetStartedBtn()){
			extentTestIOS.log(Status.PASS, "Get started button is shown");			
		} else {
			extentTestIOS.log(Status.FAIL, "Get started button is not shown");
		}
		
		//Verifying the  flow of pressing the No button flow on the Wifi Screen and Yes button on the MyQ screen
		compatibilityFlowIOS.verifyAndClickBackBtn();
		compatibilityFlowIOS.clickNoBtn();
		compatibilityFlowIOS.clickYesBtn();	
		if(compatibilityFlowIOS.verifyBuyNowBtn() && compatibilityFlowIOS.verifyHomeBridgeText()){
			extentTestIOS.log(Status.PASS, "Buy Now button for Home Bridge is shown");			
		} else {
			extentTestIOS.log(Status.FAIL, "Buy Now button for Home Bridge is not shown");
		}

		//Verifying the flow of pressing the No button on the MyQ Logo Screen
		compatibilityFlowIOS.verifyAndClickBackBtn();
		compatibilityFlowIOS.clickNoBtn();
		if(compatibilityFlowIOS.verifyBuyNowBtn() && compatibilityFlowIOS.verifySmartGarageText()){
			extentTestIOS.log(Status.PASS, "Buy now link for smart garage hub is shown");			
		} else {
			extentTestIOS.log(Status.FAIL, "Buy now link for smart garage hub is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}

		compatibilityFlowIOS.verifyAndClickExitBtn();	
	}

	/**
	 * Description	 :	This iOS test method verifies the flow of selecting the Craftsman GDO in the compatibility flow.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyingCraftsmanFlowIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestIOS = createTest(deviceTest,scenarioName);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		compatibilityFlowIOS = new  CompatibilityFlowIOS (iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		if(!compatibilityFlowIOS.verifyGDOComptabilelinkshown()) {
			baseConditionIOS("Compatibility");
		}

		compatibilityFlowIOS.clickGdoCompatibleLink();
		compatibilityFlowIOS.selectBrandGDO("Craftsman")
		.verifyAndClickExitBtn();
		if(compatibilityFlowIOS.verifyGDOComptabilelinkshown()) {
			extentTestIOS.log(Status.PASS, "GDO compatibility link is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "GDO compatibility link is not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}
		
		//Verifying the back button
		compatibilityFlowIOS.clickGdoCompatibleLink();
		compatibilityFlowIOS.selectBrandGDO("Craftsman");
		if(compatibilityFlowIOS.verifyAndClickBackBtn()) {
			extentTestIOS.log(Status.PASS, "back button is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "back button is not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}
		
		//Verifying the yes button flow on the Wifi Screen
		compatibilityFlowIOS.selectBrandGDO("Craftsman");
		compatibilityFlowIOS.clickYesBtn();
		if(compatibilityFlowIOS.verifyGetStartedBtn()){
			extentTestIOS.log(Status.PASS, "Get started button is shown");			
		} else {
			extentTestIOS.log(Status.FAIL, "Get started button is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		
		//Verifying the  flow of pressing the No button on the MyQ Logo Screen
		compatibilityFlowIOS.verifyAndClickBackBtn();
		compatibilityFlowIOS.clickNoBtn();
		if(compatibilityFlowIOS.verifyBuyNowBtn() && compatibilityFlowIOS.verifySmartGarageText()){
			extentTestIOS.log(Status.PASS, "Buy now link for Smart garage hub is shown");			
		} else {
			extentTestIOS.log(Status.FAIL, "Get started button is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		compatibilityFlowIOS.verifyAndClickExitBtn();
	}

	/**
	 * Description	 :	This iOS test method verifies the flow of selecting the Merlin GDO in the compatibility flow.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyingMerlinFlowIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestIOS = createTest(deviceTest,scenarioName);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		compatibilityFlowIOS = new  CompatibilityFlowIOS (iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		if(!compatibilityFlowIOS.verifyGDOComptabilelinkshown()) {
			baseConditionIOS("Compatibility");
		}

		compatibilityFlowIOS.clickGdoCompatibleLink();
		compatibilityFlowIOS.selectBrandGDO("Merlin")
		.verifyAndClickExitBtn();
		if(compatibilityFlowIOS.verifyGDOComptabilelinkshown()) {
			extentTestIOS.log(Status.PASS, "GDO compatibility link is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "GDO compatibility link is not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}
		
		//Verifying the back button
		compatibilityFlowIOS.clickGdoCompatibleLink();
		compatibilityFlowIOS.selectBrandGDO("Merlin");
		if(compatibilityFlowIOS.verifyAndClickBackBtn()) {
			extentTestIOS.log(Status.PASS, "back button is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "back button is not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}
		
		//Verifying the yes button flow on the Wifi Screen
		compatibilityFlowIOS.selectBrandGDO("Merlin");
		if(compatibilityFlowIOS.verifyGetStartedBtn()){
			extentTestIOS.log(Status.PASS, "Get started button is shown");			
		} else {
			extentTestIOS.log(Status.FAIL, "Get started button is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		compatibilityFlowIOS.verifyAndClickExitBtn();
	}

	/**
	 * Description	 :	This iOS test method verifies the flow of selecting the Other Brand GDO in the compatibility flow.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyingOtherFlowIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestIOS = createTest(deviceTest,scenarioName);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		compatibilityFlowIOS = new  CompatibilityFlowIOS (iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		if(!compatibilityFlowIOS.verifyGDOComptabilelinkshown()) {
			baseConditionIOS("Compatibility");
		}

		compatibilityFlowIOS.clickGdoCompatibleLink();
		compatibilityFlowIOS.selectBrandGDO("Other")
		.verifyAndClickExitBtn();
		if(compatibilityFlowIOS.verifyGDOComptabilelinkshown()) {
			extentTestIOS.log(Status.PASS, "GDO compatibility link is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "GDO compatibility link is not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}
		
		//Verifying the back button
		compatibilityFlowIOS.clickGdoCompatibleLink();
		compatibilityFlowIOS.selectBrandGDO("Other");
		if(compatibilityFlowIOS.verifyAndClickBackBtn()) {
			extentTestIOS.log(Status.PASS, "back button is shown");		
		} else {
			extentTestIOS.log(Status.FAIL, "back button is not shown");	
			utilityIOS.captureScreenshot(iOSDriver);
		}
		compatibilityFlowIOS.selectBrandGDO("Other");
		compatibilityFlowIOS.verifyAndClickExitBtn();
	}
	
	/**
	 * Description	 :	This iOS test method verifies the flow of selecting the Wifi GDO with ceiling attached in the compatibility flow.
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifyWifiGDOCeilingFlow(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String wallControl=null;
		extentTestIOS = createTest(deviceTest,scenarioName);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		deviceSetupPageIOS = new DeviceSetupPageIOS(iOSDriver, extentTestIOS);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		if(!deviceSetupPageIOS.verifyDeviceSetupTextShown()) {
			baseConditionIOS("Setup");
		}
		wallControl = testData.get(0).split(",")[0];
		deviceSerialNumber = testData.get(0).split(",")[1];
		
		deviceSetupPageIOS.clickOnWifiGDO_Ceiling();
		deviceSetupPageIOS.clickNextButton();
		//Select Wall Control
		deviceSetupPageIOS.selectWallControl(wallControl);

		//Verifying Wifi setup screen is shown and click on Next button
		if(deviceSetupPageIOS.verifyWifiSetupScreen())
		{
			extentTestIOS.log(Status.PASS, "Wifi Setup screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Wifi Setup screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickNextButton();
		
		//Verifying Beep screen is shown and click on Yes button
		if(deviceSetupPageIOS.verifyBeepScreen())
		{
			extentTestIOS.log(Status.PASS, "Beep screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Beep screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickOnYesButton();		
		
		//Verifying Wifi setup screen is shown  and click on Back button 
		if(deviceSetupPageIOS.verifyWifiSetupScreen())
		{
			extentTestIOS.log(Status.PASS, "Wifi Setup screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Wifi Setup screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickOnBackButton();
		
		//it will navigate to Beep screen again then click on didn't hear the beep link
		deviceSetupPageIOS.clickonDidnthearBeepButton();

		//Verifying Okay screen is shown and click on Next button
		if(deviceSetupPageIOS.verifyOkayScreen())
		{
			extentTestIOS.log(Status.PASS, "Okay screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Okay screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickNextButton();

		// Verifying Locate Screen Wires screen and click on Next button
		if(deviceSetupPageIOS.verifyLocateWiresScreen())
		{
			extentTestIOS.log(Status.PASS, "Locate wires screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Locate wires screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}	
		deviceSetupPageIOS.clickNextButton();
		
		//Verifying Pull Down Screen is shown and click on Next Button
		if(deviceSetupPageIOS.verifyPullDownScreen())
		{
			extentTestIOS.log(Status.PASS, "Pull down screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Pull down screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}	
		deviceSetupPageIOS.clickNextButton();
		
		//Verifying Reset Wifi Screen is shown and click on Next Button
		if(deviceSetupPageIOS.verifyResetWifiScreen())
		{
			extentTestIOS.log(Status.PASS, "Reset Wifi screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Reset Wifi screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}	
		deviceSetupPageIOS.clickNextButton();

		//Verifying Wifi Mode screen is shown and click on Next button
		if(deviceSetupPageIOS.verifyWifiModeScreen())
		{
			extentTestIOS.log(Status.PASS, "Wifi Mode screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Wifi Mode screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}	
		deviceSetupPageIOS.clickNextButton();
		
		//Verifying Beep screen is shown and click on Yes Button
		if(deviceSetupPageIOS.verifyBeepScreen())
		{
			extentTestIOS.log(Status.PASS, "Beep screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Beep screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickOnYesButton();
		
		//Verifying Serial Number screen is shown and Enter dummy serial number , click on back button
		if(deviceSetupPageIOS.verifySerialNumberScreen())
		{
			extentTestIOS.log(Status.PASS, "Serial number screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Serial number screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.enterSerialNumber(deviceSerialNumber);
		if(deviceSetupPageIOS.verifySerialNumberEntered(deviceSerialNumber)){
			extentTestIOS.log(Status.PASS, "Serial number is entered successfully");
		}else {
			extentTestIOS.log(Status.FAIL, "Serial number is not entered successfully");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickOnBackButton();
		
		//Beep screen is shown then click on dint hear beep link and Verifying second failure message
		deviceSetupPageIOS.clickonDidnthearBeepButton();
		if(deviceSetupPageIOS.verifySecondFailureError())
		{
			extentTestIOS.log(Status.PASS, "Second Failure message is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Second Failure message is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickOnBackErrorButton();
		deviceSetupPageIOS.clickOnCloseButton();
	}

	/**
	 * Description	 :	This iOS test method verifies the flow of selecting the Smart Garage Hub in the device setup page
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	public void VerifySmartGarageHubFlow(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestIOS = createTest(deviceTest,scenarioName);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		deviceSetupPageIOS = new DeviceSetupPageIOS(iOSDriver, extentTestIOS);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		if(!deviceSetupPageIOS.verifyDeviceSetupTextShown()) {
			baseConditionIOS("Setup");
		}
		deviceSerialNumber = testData.get(0).split(",")[0];
		// Click on Smart Garage Hub Device on device setup page
		deviceSetupPageIOS.clickOnSmartGarageHub();
		deviceSetupPageIOS.clickNextButton();
		deviceSetupPageIOS.clickNextButton();

		//Verifying Blue light screen is shown and click on Yes button
		if(deviceSetupPageIOS.verifyBlueLightScreen()){
			extentTestIOS.log(Status.PASS, "Blue light screen is shown");
		}else{
			extentTestIOS.log(Status.FAIL, "Blue light screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickOnYesButton();
		
		//Verifying Serial Number Screen is shown and enter dummy serial number, click on back button
		if(deviceSetupPageIOS.verifySerialNumberScreen()){
			extentTestIOS.log(Status.PASS, "Serial number screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Serial number screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.enterSerialNumber(deviceSerialNumber);	
		if(deviceSetupPageIOS.verifySerialNumberEntered(deviceSerialNumber)){
			extentTestIOS.log(Status.PASS, "Serial number is entered successfully");
		}else {
			extentTestIOS.log(Status.FAIL, "Serial number is not entered successfully");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickOnBackButton();
		
		//Blue light screen is shown , click on dint see blue light link and verifying green light screen is shown, click on Yes button
		deviceSetupPageIOS.clickOndDidntSeeBluelightLink();
		if(deviceSetupPageIOS.verifyGreenLightScreen()){
			extentTestIOS.log(Status.PASS, "Green light screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Green light screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickOnYesButton();
		
		//Verifying serial number screen is shown and enter dummy serial number , click on back button
		if(deviceSetupPageIOS.verifySerialNumberScreen()){
			extentTestIOS.log(Status.PASS, "Serial number screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Serial number screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.enterSerialNumber(deviceSerialNumber);
		if(deviceSetupPageIOS.verifySerialNumberEntered(deviceSerialNumber)){
			extentTestIOS.log(Status.PASS, "Serial number is entered successfully");
		}else {
			extentTestIOS.log(Status.FAIL, "Serial number is not entered successfully");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickOnBackButton();
		deviceSetupPageIOS.clickOndDidntSeeGreenlightLink();
		
		//Verifying Oops screen is shown and click on next button
		if(deviceSetupPageIOS.verifydOopsScreen()){
			extentTestIOS.log(Status.PASS, "Oops screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Oops screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickNextButton();
		
		//Verifying Sorry Screen steps and images are shown then click on Try Again button
		if(deviceSetupPageIOS.verifySorryScreenSteps()){
			extentTestIOS.log(Status.PASS, "Sorry screen steps images and messages are shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Sorry screen steps images and messages are not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickTryAgainButton();
		
		// Verifying Blue light screen is shown and click on Dint see blue light link
		if(deviceSetupPageIOS.verifyBlueLightScreen()){
			extentTestIOS.log(Status.PASS, "Blue light screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Blue light screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickOndDidntSeeBluelightLink();
		
		// Verifying We Apologize screen and click on close button
		if(deviceSetupPageIOS.verifyWeApologizeScreen()) {
			extentTestIOS.log(Status.PASS, "We Apologize screen is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "We Apologize screen is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}	
		//Verifying the Customer Service Call options on clicking Customer Service number
		deviceSetupPageIOS.clickCustomerServiceNumber();
		if(deviceSetupPageIOS.verifyCustomerCallOptionIsShown()) {
			extentTestIOS.log(Status.PASS, "Customer Call option is shown");
		}else {
			extentTestIOS.log(Status.FAIL, "Customer Call option is not shown");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		deviceSetupPageIOS.clickOnCloseButton();
	}
}