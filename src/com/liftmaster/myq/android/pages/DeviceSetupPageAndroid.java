package com.liftmaster.myq.android.pages;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.liftmaster.myq.utils.UtilityAndroid;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DeviceSetupPageAndroid {
	public AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;
	public String env;

	@AndroidFindBy(	xpath = "//*[@class='android.widget.Button' and @text='ALLOW']")
	public AndroidElement allow_location_button;

	@AndroidFindBy(	xpath = "//*[@class='android.widget.TextView' and @text='Device Setup']")
	public AndroidElement device_setup_text;
	
	@AndroidFindBy(id = "permission_deny_button")
	public AndroidElement deny_location_button;

	@AndroidFindBy(id = "layout_wifi_gdo")
	public AndroidElement wifi_gdo_item;

	@AndroidFindBy(id = "layout_wifi_rjo")
	public AndroidElement wifi_rjo_item;

	@AndroidFindBy(id = "layout_smart_hub")
	public AndroidElement smart_hub_item;

	@AndroidFindBy(id = "layout_myq_gateway")
	public AndroidElement ethernet_item;

	@AndroidFindBy(id = "layout_home_bridge")
	public AndroidElement home_bridge_item;

	@AndroidFindBy(id = "button_next")
	public AndroidElement what_you_need_next_button;

	@AndroidFindBy(id = "button_wall_control_next")
	public AndroidElement wall_control_next_button;

	@AndroidFindBy(id = "button_power_up_next")
	public AndroidElement power_up_next_btn;

	@AndroidFindBy(id = "button_wall_control_880lm")
	public AndroidElement wall_control_88Olm;

	@AndroidFindBy(id = "button_wall_control_886lm")
	public AndroidElement wall_control_886lm;
	
	@AndroidFindBy(id = "button_wall_control_883lm")
	public AndroidElement wall_control_883lm;
	
	@AndroidFindBy(id = "button_wall_control_888lm")
	public AndroidElement wall_control_888lm;
	
	@AndroidFindBy(id = "button_setup_next")
	public AndroidElement yes_button;

	@AndroidFindBy(id = "text_setup_link")
	public AndroidElement didnt_hear_beep_link;
	
	@AndroidFindBy(id = "text_setup_message")
	public AndroidElement beep_message_text;
	
	@AndroidFindBy(id = "text_setup_search_device")
	public AndroidElement progress_indicator_setup_search;

	@AndroidFindBy(id = "setup_loading_progress")
	public AndroidElement setup_loading_activity_indicator;

	@AndroidFindBy(id = "text_wgdo_reset_instruction")
	public AndroidElement wgdo_error_text1;

	@AndroidFindBy(id = "button_wgdo_reset_instruction")
	public AndroidElement wgdo_error_scenario_next_btn;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Locate the light cover where the wires are entering.']")
	public AndroidElement locate_wires_text;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Pull down the light cover.']")
	public AndroidElement pull_down_cover_text;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Find the @ black adjustment button located between two arrows.\n" + 
			"\n" + 
			"Press and hold the button on the garage door opener until all the button lights turn off.']")
	public AndroidElement reset_wifi_text;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Find the @ yellow learn button.\n" + 
			"\n" + 
			"Press and release the button 3 times.\n" + 
			"Listen for the beep and look for the\n" + 
			"blinking $ blue light.']")
	public AndroidElement wifi_mode_text;

	@AndroidFindBy(id = "exit")
	public AndroidElement exit_flow_button;
	
	@AndroidFindBy(id = "button1")
	public AndroidElement location_ok_button;

	@AndroidFindBy(id = "button_smart_garage_cannot_discover_ok")
	public AndroidElement sorry_page_ok_btn;
	
	@AndroidFindBy(id = "text_smart_garage_cannot_discover")
	public AndroidElement discovery_devices_sorry_text;
	
	@AndroidFindBy(id = "button_smart_garage_green_light_yes")
	public AndroidElement smart_garage_green_light_yes_btn;

	@AndroidFindBy(id = "text_smart_garage_green_light_not_shown")
	public AndroidElement  green_light_not_link;

	@AndroidFindBy(id = "button_smart_garage_unplug_retry")
	public AndroidElement  smart_garage_unplug_retry_btn;
	
	@AndroidFindBy(id = "button_smart_garage_reset_try_again")
	public AndroidElement  smart_garage_try_again_btn;
	
	@AndroidFindBy(id = "edit_setup_serial1")
	public AndroidElement first_cell_textbox;

	@AndroidFindBy(id = "edit_setup_serial2")
	public AndroidElement second_cell_textbox;

	@AndroidFindBy(id = "edit_setup_serial3")
	public AndroidElement third_cell_textbox;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='We couldn’t add your MyQ device. To fix the issue follow the steps below.']")
	public AndroidElement sorry_screen_text;

	@AndroidFindBy(id = "image_smart_garage_reset_step_one")
	public AndroidElement sorry_screen_image;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='We Apologize']")
	public AndroidElement we_apologize_heading;
	
	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='FAQ']")
	public AndroidElement faq_text;
	
	@AndroidFindBy(id = "text_call_customer_service_phone_link")
	public AndroidElement customer_care_number;
	
	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Serial Number']")
	public AndroidElement serial_number_screen;

	/**
	 * Description : class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public DeviceSetupPageAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid){
		this.androidDriver = androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);			
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid = new UtilityAndroid(androidDriver,extentTestAndroid);
	}

	/**
	 * Description : This function will click on the Next button
	 * @return Page object
	 */
	public DeviceSetupPageAndroid clickNextButtonOnWhatYouNeed() {
		try {
			utilityAndroid.clickAndroid(what_you_need_next_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click on the Yes button
	 * @return Page object
	 */
	public DeviceSetupPageAndroid clickYesButtonOnBeepScreen() {
		try {
			utilityAndroid.clickAndroid(yes_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will click on the Dint hear a beep link
	 * @return Page object
	 */
	public DeviceSetupPageAndroid clickDidntHearABeepLink() {
		try {
			utilityAndroid.clickAndroid(didnt_hear_beep_link);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will select the wall control
	 * @return Page object
	 */
	public DeviceSetupPageAndroid select880LmWallControl(String wallControl) {
		try {
			if(wallControl.equals("wallcontrol_880lm")){
			utilityAndroid.clickAndroid(wall_control_88Olm);
			}
			else if(wallControl.equals("wallcontrol_886lm")) {
				utilityAndroid.clickAndroid(wall_control_886lm);
			}
			else if(wallControl.equals("wallcontrol_883lm")) {
				utilityAndroid.clickAndroid(wall_control_883lm);
			}
			else if(wallControl.equals("wallcontrol_888lm")) {
				utilityAndroid.clickAndroid(wall_control_888lm);
			}
			
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click the next button
	 * @return Page object
	 */
	public DeviceSetupPageAndroid clickNextButtonOnWallControl() {
		try {
			utilityAndroid.clickAndroid(wall_control_next_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will wait while searching devices
	 * @return Page object
	 */
	public DeviceSetupPageAndroid waitWhileSearchingDevices() {
		try {
			for(int i =0; i<=10; i++) {
				try {
					if(progress_indicator_setup_search.isDisplayed()) {
						Thread.sleep(1000);		
					}
				}catch (Exception e) {
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will wait while loading devices
	 * @return Page object
	 */
	public DeviceSetupPageAndroid waitWhileLoadingDevices() {
		try {
			//	utilityAndroid.genericWait(setup_loading_activity_indicator,"invisible");

			for(int i =0; i<=10; i++) {
				try {
					if(setup_loading_activity_indicator.isDisplayed()) {
						Thread.sleep(1000);		
					}
				}catch (Exception e) {
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click on the Allow location button
	 * @return Page object
	 */
	public DeviceSetupPageAndroid clickAllowButton() {
		try {
			utilityAndroid.genericWait(allow_location_button, "Click");
			utilityAndroid.clickAndroid(allow_location_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function will click on the Allow location button
	 * @return Page object
	 */
	public DeviceSetupPageAndroid clickOkLocationButton() {
		try {
			utilityAndroid.genericWait(location_ok_button, "Click");
			utilityAndroid.clickAndroid(location_ok_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click on the deny location button
	 * @return Page object
	 */
	public DeviceSetupPageAndroid clickDenyButton() {
		try {
			utilityAndroid.clickAndroid(deny_location_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click on the wifi GDO from the list of hubs
	 * @return Page object
	 */
	public DeviceSetupPageAndroid selectWifiGDO() {
		try {
			utilityAndroid.clickAndroid(wifi_gdo_item);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click on the wifi RJO from the list of hubs
	 * @return Page object
	 */
	public DeviceSetupPageAndroid selectWifiRJO() {
		try {
			utilityAndroid.clickAndroid(wifi_rjo_item);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click on the smart hub from the list of hubs
	 * @return Page object
	 */
	public DeviceSetupPageAndroid selectSmartHub() {
		try {
			utilityAndroid.clickAndroid(smart_hub_item);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click on Ethernet hub from the list of hubs
	 * @return Page object
	 */
	public DeviceSetupPageAndroid selectEthernetHub() {
		try {
			utilityAndroid.clickAndroid(ethernet_item);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click on home bridge from the list of hubs
	 * @return Page object
	 */
	public DeviceSetupPageAndroid selectHomeBridge() {
		try {
			utilityAndroid.clickAndroid(home_bridge_item);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click on home bridge from the list of hubs
	 * @return Page object
	 */
	public boolean verifyWgdoErrorScreen1() {
		try {
			if(wgdo_error_text1.isDisplayed() && wgdo_error_scenario_next_btn.isDisplayed()) {
				return true;
			} else {
				utilityAndroid.captureScreenshot(androidDriver);
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function will verify that beep screen is shown
	 * @return Page object
	 */
	public boolean verifyBeepScreenShown() {
		try {
			if(beep_message_text.isDisplayed() ) {
				return true;
			} else {
				utilityAndroid.captureScreenshot(androidDriver);
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function will click the next button on Error Scenarios for Wi-Fi GDO
	 * @return Page object
	 */
	public DeviceSetupPageAndroid clickNextButtonErrorScreens() {
		try {
			utilityAndroid.clickAndroid(wgdo_error_scenario_next_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function is to click on Locate Wires WGDO screen
	 * @return boolean
	 */
	public boolean verifyLocateWiresWGDOScreen() {
		try {
			if(locate_wires_text.isDisplayed()) {
				return true;
			} else {
				utilityAndroid.captureScreenshot(androidDriver);
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function is to Verify Pull Down WGDO screen
	 * @return boolean
	 */
	public boolean verifyPullDownWgdoScreen() {
		try {
			if(pull_down_cover_text.isDisplayed()) {
				return true;
			} else {
				utilityAndroid.captureScreenshot(androidDriver);
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function is to Verify Reset Wifi WGDO screen
	 * @return boolean
	 */
	public boolean verifyResetWiFiWgdoScreen() {
		try {
			if(reset_wifi_text.isDisplayed()) {
				return true;
			} else {
				utilityAndroid.captureScreenshot(androidDriver);
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function is to verify FAQ screen is shown
	 * @return boolean
	 */
	public boolean verifyFAQScreen() {
		try {
			if(faq_text.isDisplayed()) {
				return true;
			} else {
				utilityAndroid.captureScreenshot(androidDriver);
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : This function is to verify WiFi mode WGDO screen
	 * @return Page object
	 */
	public boolean verifyWifiModeWgdoScreen() {
		try {
			if(wifi_mode_text.isDisplayed()) {
				return true;
			} else {
				utilityAndroid.captureScreenshot(androidDriver);
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function is to click on exit flow button
	 * @return Page object
	 */
	public DeviceSetupPageAndroid exitSetupFlow() {
		try {
			utilityAndroid.clickAndroid(exit_flow_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function is to click on Next button on Power Up screen
	 * @return Page object
	 */
	public DeviceSetupPageAndroid clickNextBtnOnPowerUp() {
		try {
			utilityAndroid.clickAndroid(power_up_next_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function is to click on OK button on Sorry page
	 * @return Page object
	 */
	public DeviceSetupPageAndroid clickOkButtonOnSorryPage() {
		try {
			utilityAndroid.clickAndroid(sorry_page_ok_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function is to verify Discovery Devices Sorry Screen
	 * @return boolean
	 */
	public boolean verifyDiscoveryDevicesSorryScreen() {
		try {
			String err_message="Sorry we still couldn\'t discover any devices.\n"+
							   "\n"+
							   "Let’s try something else. Tap OK below.";
			utilityAndroid.genericWait(discovery_devices_sorry_text, "Visible");
			if(discovery_devices_sorry_text.isDisplayed() && sorry_page_ok_btn.isDisplayed() && err_message.equals(discovery_devices_sorry_text.getText())) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function is to click on Yes button on Green light page
	 * @return boolean
	 */
	public DeviceSetupPageAndroid clickYesGreenLightSmartGarageHub() {
		try {
			utilityAndroid.clickAndroid(smart_garage_green_light_yes_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function is to click on Don't see Green light link
	 * @return boolean
	 */
	public DeviceSetupPageAndroid clickDontSeeGreenLightLink() {
		try {
			utilityAndroid.clickAndroid(green_light_not_link);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function is to click on Retry button on Unplug screen
	 * @return boolean
	 */
	public DeviceSetupPageAndroid clickUnplugRetryButton() {
		try {
			utilityAndroid.clickAndroid(smart_garage_unplug_retry_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function is Verify Unable to Add Sorry page
	 * @return boolean
	 */
	public boolean verifyUnableToAddSorryPage() {
		try {
			if(sorry_screen_image.isDisplayed() && sorry_screen_text.isDisplayed()) {
				return true;
			} else {
				return false;	
			}
		
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : This function is to verify Apologize Page
	 * @return boolean
	 */
	public boolean verifyApologizePage() {
		try {
			if(we_apologize_heading.isDisplayed() && customer_care_number.isDisplayed()) {
				return true;
			} else {
				return false;	
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function is to verify Serial Number page
	 * @return boolean
	 */
	public boolean verifySerialNumberPage() {
		try {
			if(serial_number_screen.isDisplayed()) {
				return true;
			} else {
				return false;	
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function is to verify Serial Number page
	 * @return boolean
	 */
	public boolean verifyDevicesSetupScreen() {
		try {
			if(device_setup_text.isDisplayed()) {
				return true;
			} else {
				return false;	
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}
	
	/**
	 * Description : This function is to click on Try Again button
	 * @return boolean
	 */
	public DeviceSetupPageAndroid clickTryAgain() {
		try {
			utilityAndroid.swipeUp();
			utilityAndroid.clickAndroid(smart_garage_try_again_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function is to enter serial number into Serial Number Screen textboxes
	 * @return boolean
	 */
	public DeviceSetupPageAndroid enterSerialNumber(String serialNumber) {
        String result = "";
        String result2 = "";
        String line = "";
        StringBuffer output = new StringBuffer();
		try {
			String MyQSrNumber = serialNumber.substring(2);
			String textBox1 = MyQSrNumber.split(" ")[0].substring(0, 4);
			String textBox2 = MyQSrNumber.split(" ")[0].substring(4, 7);
			String textBox3 = MyQSrNumber.split(" ")[0].substring(7);

			utilityAndroid.genericWait(first_cell_textbox, "Click");
			utilityAndroid.clearAndroid(first_cell_textbox);
			
			//adb command to get the current(default) device keyboard type
            Process p = Runtime.getRuntime().exec("adb shell settings get secure default_input_method");
            //below code is to convert the output of the command execute to string
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            while ((line = stdInput.readLine())!= null) {
                output.append(line + "\n");
            }
            result = output.toString();
            //adb command to set the unicode keyboard
            Runtime.getRuntime().exec("adb shell ime set io.appium.android.ime/.UnicodeIME");
            Thread.sleep(2000);
            //to enable entering through unicode keyboard
            Runtime.getRuntime().exec("adb shell input keyevent 23");
            
			utilityAndroid.enterTextAndroid(first_cell_textbox, textBox1);
			utilityAndroid.genericWait(second_cell_textbox, "Click");
			utilityAndroid.enterTextAndroid(second_cell_textbox, textBox2);
			utilityAndroid.genericWait(third_cell_textbox, "Click");
			utilityAndroid.enterTextAndroid(third_cell_textbox, textBox3);
			
			// concat the value of default keyboard to adb command, to set back the to original
            String command = "adb shell ime set ".concat(result);
            //execute the command to set the keyboard to device default
            Process p2 = Runtime.getRuntime().exec(command);

            BufferedReader stdInput1 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
            while ((line = stdInput1.readLine())!= null) {
                output.append(line + "\n");
            }
            result2 = output.toString();
		}
		catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function is to click on Try Again button
	 * @return boolean
	 */
	public boolean verifySerialNumberEntered(String serialNumber) {
		try {
			String concatSerialNumber=first_cell_textbox.getText()+second_cell_textbox.getText()+third_cell_textbox.getText();
			if(concatSerialNumber.equalsIgnoreCase(serialNumber.substring(2))){
				return true;
			}else {
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;	
		}
	}
}