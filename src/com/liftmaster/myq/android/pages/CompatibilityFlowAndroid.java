package com.liftmaster.myq.android.pages;

import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.liftmaster.myq.utils.UtilityAndroid;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class CompatibilityFlowAndroid {
	private AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;
	public String env;

	//Page factory for Login Page Android
	@AndroidFindBy(id = "button_startup_get_started")
	public AndroidElement startup_get_started_btn;

	@AndroidFindBy(id = "button_compatibility_get_started")
	public AndroidElement compatible_get_started_btn;

	@AndroidFindBy(id = "text_startup_door_compatibility")
	public AndroidElement gdo_compatibility_link;

	@AndroidFindBy(id = "text_startup_sign_in")
	public AndroidElement sign_in_btn;

	@AndroidFindBy(className = "android.widget.RelativeLayout")
	public AndroidElement brands_list;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Smart Garage Hub']")
	public AndroidElement smart_garage_hub_text;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Home Bridge']")
	public AndroidElement home_bridge_text;

	@AndroidFindBy(id = "button_compatibility_buy_now")
	public AndroidElement buy_now_btn;

	@AndroidFindBy(id = "button_compatibility_wifi_yes")
	public AndroidElement wifi_yes_btn;

	@AndroidFindBy(id = "button_compatibility_wifi_no")
	public AndroidElement wifi_no_btn;

	@AndroidFindBy(id = "button_compatibility_myq_yes")
	public AndroidElement myq_yes_btn;

	@AndroidFindBy(id = "button_compatibility_myq_no")
	public AndroidElement myq_no_btn;

	@AndroidFindBy(id = "exit")
	public AndroidElement exit_btn;

	@AndroidFindBy(xpath = "//*[@class='android.widget.ImageButton' and @content-desc='Navigate up']" )
	public AndroidElement back_button;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Welcome.']")
	public AndroidElement splash_screen1;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Open when you want.']")
	public AndroidElement splash_screen2;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Always in the know.']")
	public AndroidElement splash_screen3;

	/**
	 * Description : class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public CompatibilityFlowAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid){
		this.androidDriver = androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);			
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid = new UtilityAndroid(androidDriver,extentTestAndroid);
	}

	/**
	 * Description : This function will click on the GDO compatible link
	 * @return Page object
	 */
	public CompatibilityFlowAndroid clickGdoCompatibleLink() {
		try {
			utilityAndroid.genericWait(gdo_compatibility_link , "Click");
			utilityAndroid.clickAndroid(gdo_compatibility_link);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : To select the GDO brand from the list of brands
	 * @param brandName
	 * @throws InterruptedException
	 * @return Page object
	 */
	public CompatibilityFlowAndroid selectBrandGDO(String brandName) throws InterruptedException {
		try {
			int noOfBrands = brands_list.findElementsByClassName("android.widget.Button").size();
			for (int iVal = 0; iVal < noOfBrands; iVal++) {
				String currentBrand =  brands_list.findElementsByClassName("android.widget.Button").get(iVal).getAttribute("resourceId");
				currentBrand = currentBrand.split(":id/")[1];
				if (currentBrand.toUpperCase().contains(brandName.toUpperCase())) {
					utilityAndroid.clickAndroid(brands_list.findElementsByClassName("android.widget.Button").get(iVal));
					break;
				}
			}
			return this;
		} catch (Exception e){
			utilityAndroid.catchExceptions(androidDriver, e);
			return this;
		}
	}

	/**
	 * Description : To verify the number of brands 
	 * @throws InterruptedException
	 * @return boolean
	 */
	public boolean verifyNumberOfBrands() throws InterruptedException {
		try {
			boolean otherbrandShown = false, liftmasterBrandShown = false, chamberlainBrandShown = false, merlinBrandShown = false, craftsManShown = false;
			int noOfBrands = brands_list.findElementsByClassName("android.widget.Button").size();

			for (int iVal = 0; iVal < noOfBrands; iVal++) { 
				switch(iVal) {
				case 0:
					if(brands_list.findElementsByClassName("android.widget.Button").get(iVal).getAttribute("resourceId").split(":id/")[1].toUpperCase().contains("CHAMBERLAIN")) {
						chamberlainBrandShown = true;	
						break;
					} else {
						chamberlainBrandShown = false;	 
					}
				case 1:
					if(brands_list.findElementsByClassName("android.widget.Button").get(iVal).getAttribute("resourceId").split(":id/")[1].toUpperCase().contains("LIFTMASTER")) {
						liftmasterBrandShown = true;	 
						break;
					}  else {
						liftmasterBrandShown = false;
					}
				case 2:
					if(brands_list.findElementsByClassName("android.widget.Button").get(iVal).getAttribute("resourceId").split(":id/")[1].toUpperCase().contains("MERLIN")) {
						merlinBrandShown = true;	 
						break;
					} else {
						merlinBrandShown =  false;
					}
				case 3:
					if(brands_list.findElementsByClassName("android.widget.Button").get(iVal).getAttribute("resourceId").split(":id/")[1].toUpperCase().contains("CRAFTSMAN")) {
						craftsManShown = true;	 
						break;
					} else {
						craftsManShown = false;
					}
				case 4:
					if(brands_list.findElementsByClassName("android.widget.Button").get(iVal).getAttribute("resourceId").split(":id/")[1].toUpperCase().contains("OTHER")) {
						otherbrandShown =true;	 
					} else {
						otherbrandShown = false;	
						break;
					}
				}
			}
			if(liftmasterBrandShown && chamberlainBrandShown && merlinBrandShown && craftsManShown &&otherbrandShown) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e){
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**	Description: To verify if Buy Now button is displayed
	 * @return boolean
	 */
	public boolean verifyBuyNowBtn( ) { 
		try {
			if(buy_now_btn.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**	Description: To verify that the smart garage text is shown
	 * @return boolean
	 */
	public boolean verifySmartGarageText() { 
		try {
			if(smart_garage_hub_text.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}	

	/**	Description: To verify that home bridge text is shown
	 * @return boolean
	 */
	public boolean verifyHomeBridgeText() { 
		try {
			if(home_bridge_text.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}	

	/**	Description: To click on Yes button on Wi-Fi GDO confirmation screen
	 * @return Page object
	 */
	public CompatibilityFlowAndroid clickWiFiYesBtn( ) { 
		try {
			utilityAndroid.clickAndroid(wifi_yes_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**	Description: To click on No button on Wi-Fi GDO confirmation screen
	 * @return Page object
	 */
	public CompatibilityFlowAndroid clickWiFiNoBtn( ) { 
		try {
			utilityAndroid.clickAndroid(wifi_no_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**	Description: To click on Yes button on MyQ Logo screen 
	 * @return Page object
	 */
	public CompatibilityFlowAndroid clickMyqYesBtn( ) { 
		try {
			utilityAndroid.clickAndroid(myq_yes_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**	Description: To click on No button on MyQ logo screen
	 * @return Page object
	 */
	public CompatibilityFlowAndroid clickMyqNoBtn( ) { 
		try {
			utilityAndroid.clickAndroid(myq_no_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**	Description: To verify get started button is shown on Home screen
	 * @return boolean
	 */
	public boolean verifyGetStartedBtnOnStartUp( ) { 
		try {
			if(startup_get_started_btn.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**	Description: To verify get started button is shown on compatibility flow
	 * @return boolean
	 */
	public boolean verifyGetStartedBtnOnCompatibleFlow( ) { 
		try {
			if(compatible_get_started_btn.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**	Description: To verify if the GDO compatible Link is shown
	 * @return boolean
	 */
	public boolean verifyGDOComptabilelinkShown( ) { 
		try {
			if(gdo_compatibility_link.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**	Description: To verify the sign in button is shown on the splash screen
	 * @return boolean
	 */
	public boolean verifySigninBtnShown( ) { 
		try {
			if(sign_in_btn.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**	Description:To verify and click on the back button
	 * @return boolean
	 */
	public boolean verifyAndClickBackBtn() { 
		try {
			if(back_button.isDisplayed()) {
				utilityAndroid.clickAndroid(back_button);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**	Description:To verify and click on the exit button
	 * @return boolean
	 */
	public boolean verifyAndClickExitBtn() { 
		try {
			if(exit_btn.isDisplayed()) {
				utilityAndroid.clickAndroid(exit_btn);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**	Description:To verify that all 3 splash screen are shown
	 * @return boolean
	 */
	public boolean verifyAllSplashScreen() {
		try {
			boolean splashScreenShown1;
			boolean splashScreenShown2;
			boolean splashScreenShown3;
			utilityAndroid.genericWait(splash_screen1 , "Visible");
			if(splash_screen1.isDisplayed()) {
				splashScreenShown1 = true;
			} else {
				splashScreenShown1 = false;
				utilityAndroid.captureScreenshot(androidDriver);
			}
			utilityAndroid.genericWait(splash_screen2 , "Visible");
			if(splash_screen2.isDisplayed()) {
				splashScreenShown2 = true;
			} else {
				splashScreenShown2 = false;
				utilityAndroid.captureScreenshot(androidDriver);
			}
			utilityAndroid.genericWait(splash_screen2 , "Visible");
			if(splash_screen3.isDisplayed()) {
				splashScreenShown3 = true;
			} else {
				splashScreenShown3 = false;
				utilityAndroid.captureScreenshot(androidDriver);
			}
			if(splashScreenShown1 && splashScreenShown2 && splashScreenShown3) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e){
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}
}
